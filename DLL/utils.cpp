#include "utils.h"
#define BUFSIZE 512

// Logs info/errors by sending the supplied buffer to the DebugView. 
// Such info is visible to the DEBUGGER (if attached), or via DebugView utility 
// of Sysinternals when no debugger is attached.
void Log(const TCHAR* error, bool is_error) {

#define TEX_LEN 1024
#define ERROR_MARKER _T("XXX Injected DLL error XXX:")
#define STD_MARKER _T("___ Injected DLL info ___:")
	TCHAR buff[TEX_LEN + MAX_PATH];
	DWORD pid, tid;
	TCHAR module_path[MAX_PATH];

	// First of all, retrieve some info about the current process
	pid = GetCurrentProcessId();
	tid = GetCurrentThreadId();
	if (GetModuleFileName(NULL, module_path, MAX_PATH - 1) == 0) {
		// FAILED.
		swprintf_s(module_path, MAX_PATH, _T("%s"), _T("NOT_AVAILABLE"));
	}

	swprintf_s(buff, TEX_LEN + MAX_PATH, _T("%s %s PID: %d TID: %d PATH: %s"), is_error ? ERROR_MARKER : STD_MARKER, error, pid, tid, module_path);
	OutputDebugString(buff);
};

BOOL is_directory_objattr(const OBJECT_ATTRIBUTES *obj)
{
	static NTSTATUS(WINAPI *pNtQueryAttributesFile)(
		_In_   const OBJECT_ATTRIBUTES *ObjectAttributes,
		_Out_  PFILE_BASIC_INFORMATION FileInformation
		);

	if (pNtQueryAttributesFile == NULL) {
		*(FARPROC *)&pNtQueryAttributesFile = GetProcAddress(
			GetModuleHandle(_T("ntdll")), "NtQueryAttributesFile");
	}

	FILE_BASIC_INFORMATION basic_information;
	if (NT_SUCCESS(pNtQueryAttributesFile(obj, &basic_information))) {
		return basic_information.FileAttributes & FILE_ATTRIBUTE_DIRECTORY;
	}
	return FALSE;
}

int is_ignored_file_unicode(const wchar_t *fname, int length)
{
	struct _ignored_file_t *f = g_ignored_files;
	for (unsigned int i = 0; i < ARRAYSIZE(g_ignored_files); i++, f++) {
		if (f->flags == FLAG_NONE && length == f->length &&
			!wcsnicmp(fname, f->unicode, length)) {
			return 1;
		}
		else if (f->flags == FLAG_BEGINS_WITH && length >= f->length &&
			!wcsnicmp(fname, f->unicode, f->length)) {
			return 1;
		}
	}
	return 0;
}

/*
uint32_t path_from_handle(HANDLE handle,
	wchar_t *path, uint32_t path_buffer_len)
{
	static NTSTATUS(WINAPI *pNtQueryVolumeInformationFile)(
		_In_   HANDLE FileHandle,
		_Out_  PIO_STATUS_BLOCK IoStatusBlock,
		_Out_  PVOID FsInformation,
		_In_   ULONG Length,
		_In_   FS_INFORMATION_CLASS FsInformationClass
		);

	if (pNtQueryVolumeInformationFile == NULL) {
		*(FARPROC *)&pNtQueryVolumeInformationFile = GetProcAddress(
			GetModuleHandle(_T("ntdll")), "NtQueryVolumeInformationFile");
	}

	static NTSTATUS(WINAPI *pNtQueryInformationFile)(
		_In_   HANDLE FileHandle,
		_Out_  PIO_STATUS_BLOCK IoStatusBlock,
		_Out_  PVOID FileInformation,
		_In_   ULONG Length,
		_In_   FILE_INFORMATION_CLASS FileInformationClass
		);

	if (pNtQueryInformationFile == NULL) {
		*(FARPROC *)&pNtQueryInformationFile = GetProcAddress(
			GetModuleHandle(_T("ntdll")), "NtQueryInformationFile");
	}

	IO_STATUS_BLOCK status = {};
	FILE_FS_VOLUME_INFORMATION volume_information;

	unsigned char buf[FILE_NAME_INFORMATION_REQUIRED_SIZE];
	FILE_NAME_INFORMATION *name_information = (FILE_NAME_INFORMATION *)buf;

	// get the volume serial number of the directory handle
	if (NT_SUCCESS(pNtQueryVolumeInformationFile(handle, &status,
		&volume_information, sizeof(volume_information),
		FileFsVolumeInformation)) == 0) {
		return 0;
	}

	unsigned long serial_number;

	// enumerate all harddisks in order to find the
	// corresponding serial number
	wcscpy(path, L"?:\\");
	for (path[0] = 'A'; path[0] <= 'Z'; path[0]++) {
		if (GetVolumeInformationW(path, NULL, 0, &serial_number, NULL,
			NULL, NULL, 0) == 0 ||
			serial_number != volume_information.VolumeSerialNumber) {
			continue;
		}

		// obtain the relative path for this filename on the given harddisk
		if (NT_SUCCESS(pNtQueryInformationFile(handle, &status,
			name_information, FILE_NAME_INFORMATION_REQUIRED_SIZE,
			FileNameInformation))) {

			uint32_t length =
				name_information->FileNameLength / sizeof(wchar_t);

			// NtQueryInformationFile omits the "C:" part in a
			// filename, apparently
			wcsncpy(path + 2, name_information->FileName,
				path_buffer_len - 2);

			return length + 2 < path_buffer_len ?
				length + 2 : path_buffer_len - 1;
		}
	}
	return 0;
}
*/

// Given a valid HANDLE to a registry key, this function will retrieve the associated path to the key. Such path is then stored into the
// buffer pointed by path, and the length of the path is returned.
// In case of failure 0 is returned.
uint32_t registry_path_from_handle(HANDLE KeyHandle, wchar_t *path, uint32_t tchars_in_buff) {
	// Retrieve the path to the ROOT handle
	DWORD size = 0;
	DWORD result = 0;

	// If this is the first time we are using NtQueryKey, retrieve its address
	if (NtQueryKey == NULL) {
		HMODULE module = LoadLibrary(L"ntdll.dll");
		NtQueryKey = (lpNtQueryKey)GetProcAddress(module, "NtQueryKey");
		if (NtQueryKey == NULL) {
			// This is a severe error
			OutputDebugString(_T("X0X0 ERROR: cannot find the address of NtQueryInformationFile"));
			return 0;
		}
	}

	// NtQueryKey needs a buffer to store data into. We need to first query "how much" data do we need
	result = NtQueryKey(KeyHandle, 3 /* KeyNameInformation */, NULL, 0, &size);
	
	// Now we need to allocate the buffer and invoke the function again. Note that we also provide space for the termination character.
	size += sizeof(wchar_t); // Add space for terminator
	wchar_t* buffer = new wchar_t[size / sizeof(wchar_t)]; // size is in bytes, we allocate wchars (=2bytes each)
	result = NtQueryKey(KeyHandle, 3 /* KeyNameInformation */, buffer, size, &size);
	if (result != STATUS_SUCCESS) {
		delete[] buffer;
		OutputDebugString(_T("X0X0 ERROR: error when invoking NtQueryKey"));
		return 0;
	}

	// NtQueryKey also puts a leading ULONG value indicating the length of the following string: 
	// https://msdn.microsoft.com/en-us/library/windows/hardware/ff553381(v=vs.85).aspx
	// but does not terminate the string with the terminator. Let's do this here.
	buffer[size / sizeof(wchar_t)] = L'\0';

	// We can finally copy the string to the output buffer
	swprintf_s(path, tchars_in_buff, L"%s", buffer + 2 /*ignore string len*/);
	delete[] buffer;

	// Return the length in wchars of the string
	return wcsnlen_s(path, tchars_in_buff);
}

// Given object attributes, calculate the NT path of the registry key pointed by the structure (e.g. \REGISTRY\MACHINE\...).
// The calculated string is written into the provided buffer. The function assumes its length in wchars
// is passed via tchars_in_buff parameter. 
// The function returns the string length of the calculated path. In case of failure, it returns 0.
uint32_t registry_path_from_object_attributes(const OBJECT_ATTRIBUTES *obj,
	wchar_t *path, uint32_t tchars_in_buff)
{
	if (obj->ObjectName == NULL || obj->ObjectName->Buffer == NULL) {
		return 0;
	}

	// ObjectName->Length is actually the size in bytes.
	uint32_t obj_length = obj->ObjectName->Length / sizeof(wchar_t);

	// The path can either be:
	// 1. A fully qualified pathname (in such case it is contained in ObjectName and starts with \Registry)
	// 2. A path name relative to another registry key, of which handle is contained in RootDirectory

	// Case 1.
	// TODO: Test a case in which we only take the following branch
	if (obj->RootDirectory == NULL) {
		wcsncpy(path, obj->ObjectName->Buffer, tchars_in_buff);
		
		// make sure to provide a valid null-terminated string
		if (obj_length > tchars_in_buff) {
			path[tchars_in_buff - 1] = '\0';
			return tchars_in_buff;
		} else
			return obj_length > tchars_in_buff ? tchars_in_buff : obj_length;
	}

	// Case 2
	// Retrieve the path to the ROOT handle
	DWORD size = 0;
	DWORD result = 0;
	
	// If this is the first time we are using NtQueryKey, retrieve its address
	if (NtQueryKey == NULL) {
		HMODULE module = LoadLibrary(L"ntdll.dll");
		NtQueryKey = (lpNtQueryKey)GetProcAddress(module, "NtQueryKey");
		if (NtQueryKey == NULL) {
			// This is a severe error
			OutputDebugString(_T("X0X0 ERROR: cannot find the address of NtQueryInformationFile"));
			return 0;
		}
	}
	
	// NtQueryKey needs a buffer to store data into. We need to first query "how much" data do we need
	result = NtQueryKey(obj->RootDirectory, 3 /* KeyNameInformation */, NULL, 0, &size);
	// Now we need to allocate the buffer and invoke the function again. Note that we also provide space for the termination character.
	size += sizeof(wchar_t); // Add space for terminator
	wchar_t* buffer = new wchar_t[size / sizeof(wchar_t)]; // size is in bytes, we allocate wchars (=2bytes each)
	result = NtQueryKey(obj->RootDirectory, 3 /* KeyNameInformation */, buffer, size, &size);
	if (result != STATUS_SUCCESS) {
		delete[] buffer;
		OutputDebugString(_T("X0X0 ERROR: error when invoking NtQueryKey"));
		return 0;
	}

	// NtQueryKey also puts a leading ULONG value indicating the length of the following string: https://msdn.microsoft.com/en-us/library/windows/hardware/ff553381(v=vs.85).aspx
	// but does not terminate the string with the terminator. Let's do this here.
	buffer[size / sizeof(wchar_t)-1] = L'\0'; 
	// We can finally copy the string to the output buffer
	swprintf_s(path, tchars_in_buff, L"%s\\%s", buffer + 2 /*ignore string len*/, obj->ObjectName->Buffer);
	delete[] buffer;

	// Return the length in wchars of the string
	return (size - sizeof(ULONG) + obj->ObjectName->Length) / sizeof(wchar_t);
}


uint32_t path_from_object_attributes(const OBJECT_ATTRIBUTES *obj,
	TCHAR *path, uint32_t buffer_length)
{
	if (obj->ObjectName == NULL || obj->ObjectName->Buffer == NULL) {
		return 0;
	}

	// ObjectName->Length is actually the size in bytes.
	uint32_t obj_length = obj->ObjectName->Length / sizeof(wchar_t);

	if (obj->RootDirectory == NULL) {
		wcsncpy(path, obj->ObjectName->Buffer, buffer_length);
		return obj_length > buffer_length ? buffer_length : obj_length;
	}

	uint32_t length = GetFinalPathNameByHandle(obj->RootDirectory,
		path, buffer_length, FILE_NAME_NORMALIZED);

	path[length++] = L'\\';
	wcsncpy(&path[length], obj->ObjectName->Buffer, buffer_length - length);

	length += obj_length;
	return length > buffer_length ? buffer_length : length;
}

int ensure_absolute_path(wchar_t *out, const wchar_t *in, int length)
{
	if (!wcsncmp(in, L"\\??\\", 4)) {
		length -= 4, in += 4;
		wcsncpy(out, in, length < MAX_PATH ? length : MAX_PATH);
		return out[length] = 0, length;
	}
	else if (in[1] != ':' || (in[2] != '\\' && in[2] != '/')) {
		wchar_t cur_dir[MAX_PATH], fname[MAX_PATH];
		GetCurrentDirectoryW(ARRAYSIZE(cur_dir), cur_dir);

		// ensure the filename is zero-terminated
		wcsncpy(fname, in, length < MAX_PATH ? length : MAX_PATH);
		fname[length] = 0;

		PathCombineW(out, cur_dir, fname);
		return lstrlenW(out);
	}
	else {
		wcsncpy(out, in, length < MAX_PATH ? length : MAX_PATH);
		return out[length] = 0, length;
	}
}

// Checks whether the passed ACCESS_MASK has any of the WRITE bits set.
bool access_mask_write_perm(const ACCESS_MASK mask) {
	return
		(mask & GENERIC_ALL) == GENERIC_ALL ||
		(mask & GENERIC_WRITE) == GENERIC_WRITE ||
		(mask & FILE_GENERIC_WRITE) == FILE_GENERIC_WRITE ||
		(mask & DELETE) == DELETE || 
		(mask & WRITE_DAC) == WRITE_DAC || 
		(mask & WRITE_OWNER) == WRITE_OWNER ||
		(mask & FILE_ALL_ACCESS) == FILE_ALL_ACCESS ||
		(mask & FILE_APPEND_DATA) == FILE_APPEND_DATA ||
		(mask & FILE_WRITE_ATTRIBUTES) == FILE_WRITE_ATTRIBUTES ||
		(mask & FILE_WRITE_DATA) == FILE_WRITE_DATA ||
		(mask & FILE_WRITE_EA) == FILE_WRITE_EA ||
		(mask & STANDARD_RIGHTS_WRITE) == STANDARD_RIGHTS_WRITE;
}

// Checks whether the passed ACCESS_MASK has any of the EXECUTE bits set. 
bool access_mask_execute_perm(ACCESS_MASK mask) {
	return (mask & FILE_EXECUTE) == FILE_EXECUTE;
}

// Given a filename, this function will calculate both MD5 and SHA1 hashes on that. Results are written in the
// FileHash struct passed by reference.
bool calc_file_hashes(__in LPCTSTR filename, __out FileHashes* hash_res) {
	BYTE file_buf[4096];
	DWORD dwStatus = 0;
	BOOL bResult = FALSE;
	HCRYPTPROV hProv = 0;
	HCRYPTHASH sha1_hasher = 0;
	HCRYPTHASH md5_hasher = 0;
	HANDLE hFile = NULL;
	DWORD cbRead = 0;
	DWORD cbHash = 0;
	char rgbDigits[] = "0123456789abcdef";

	// Initialize the hash_res structure
	ZeroMemory(hash_res, sizeof(FileHashes));

	if (hash_res == NULL)
		return false;

	if (filename == NULL)
		return false;

	// Open the file for reading and start calculating hashes
	hFile = CreateFile(filename,
		GENERIC_READ,
		FILE_SHARE_READ| FILE_SHARE_WRITE | FILE_SHARE_DELETE,
		NULL,
		OPEN_EXISTING,
		FILE_FLAG_SEQUENTIAL_SCAN,
		NULL);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		hash_res->file_existed = 0;
		return false;
	}
	else {
		hash_res->file_existed = 1;
	}

	// Get handle to both MD5 and SHA1 crypto providers
	if (!CryptAcquireContext(&hProv,
		NULL,
		NULL,
		PROV_RSA_FULL,
		CRYPT_VERIFYCONTEXT))
	{
		CloseHandle(hFile);
		return false;
	}

	if (!CryptCreateHash(hProv, CALG_SHA1, 0, 0, &sha1_hasher))
	{
		CloseHandle(hFile);
		CryptReleaseContext(hProv, 0);
		return false;
	}

	if (!CryptCreateHash(hProv, CALG_MD5, 0, 0, &md5_hasher))
	{
		CloseHandle(hFile);
		CryptReleaseContext(hProv, 0);
		CryptDestroyHash(sha1_hasher);
		return false;
	}

	while (bResult = ReadFile(hFile, file_buf, BUFSIZE,
		&cbRead, NULL))
	{
		hash_res->size += cbRead;
		if (0 == cbRead)
		{
			break;
		}

		if (!CryptHashData(sha1_hasher, file_buf, cbRead, 0))
		{
			CryptReleaseContext(hProv, 0);
			CryptDestroyHash(sha1_hasher);
			CryptDestroyHash(md5_hasher);
			CloseHandle(hFile);
			return false;
		}

		if (!CryptHashData(md5_hasher, file_buf, cbRead, 0))
		{
			CryptReleaseContext(hProv, 0);
			CryptDestroyHash(sha1_hasher);
			CryptDestroyHash(md5_hasher);
			CloseHandle(hFile);
			return false;
		}
	}

	if (!bResult)
	{
		CryptReleaseContext(hProv, 0);
		CryptDestroyHash(sha1_hasher);
		CryptDestroyHash(md5_hasher);
		CloseHandle(hFile);
		return false;
	}

	// Now retrieve the hashed values
	cbHash = 20;
	if (!CryptGetHashParam(sha1_hasher, HP_HASHVAL, hash_res->sha1_digest, &cbHash, 0))
	{
		dwStatus = GetLastError();
		printf("CryptCreateHash failed retrieving SHA1 digest: %d\n", dwStatus);
		CryptDestroyHash(sha1_hasher);
		CryptDestroyHash(md5_hasher);
		CryptReleaseContext(hProv, 0);
		CloseHandle(hFile);
		return false;
	}

	cbHash = 16;
	if (!CryptGetHashParam(md5_hasher, HP_HASHVAL, hash_res->md5_digest, &cbHash, 0))
	{
		dwStatus = GetLastError();
		printf("CCryptCreateHash failed retrieving MD5 digest: %d\n", dwStatus);
		CryptDestroyHash(sha1_hasher);
		CryptDestroyHash(md5_hasher);
		CryptReleaseContext(hProv, 0);
		CloseHandle(hFile);
		return false;
	}

	CryptDestroyHash(sha1_hasher);
	CryptDestroyHash(md5_hasher);
	CryptReleaseContext(hProv, 0);
	CloseHandle(hFile);

	// Now convert digests into hex strings
	for (DWORD i = 0; i < 20; i++)
	{
		swprintf(hash_res->sha1_hex + i * 2, _T("%c%c"), rgbDigits[hash_res->sha1_digest[i] >> 4], rgbDigits[hash_res->sha1_digest[i] & 0xf]);
	}
	hash_res->sha1_hex[40] = '\0';
	for (DWORD i = 0; i < 16; i++)
	{
		swprintf(hash_res->md5_hex + i * 2, _T("%c%c"), rgbDigits[hash_res->md5_digest[i] >> 4], rgbDigits[hash_res->md5_digest[i] & 0xf]);
	}
	hash_res->md5_hex[32] = '\0';

	return true;
}

NTSTATUS NtQueryValueKey(
	_In_      HANDLE                      KeyHandle,
	_In_      PUNICODE_STRING             ValueName,
	_In_      KEY_VALUE_INFORMATION_CLASS KeyValueInformationClass,
	_Out_opt_ PVOID                       KeyValueInformation,
	_In_      ULONG                       Length,
	_Out_     PULONG                      ResultLength
	) {
	if (_NtQueryValueKey == NULL) {
		HMODULE module = LoadLibrary(L"ntdll.dll");
		_NtQueryValueKey = (lpNtQueryValueKey)GetProcAddress(module, "NtQueryValueKey");
		if (_NtQueryValueKey == NULL) {
			// This is a severe error
			OutputDebugString(_T("X0X0 ERROR: cannot find the address of NtQueryValueKey"));
			return 0;
		}
	}
	return _NtQueryValueKey(KeyHandle, ValueName, KeyValueInformationClass, KeyValueInformation, Length, ResultLength);
}

NTSTATUS NtOpenKey(
	_Out_ PHANDLE            KeyHandle,
	_In_  ACCESS_MASK        DesiredAccess,
	_In_  POBJECT_ATTRIBUTES ObjectAttributes
	) {
	if (_NtOpenKey == NULL) {
		HMODULE module = LoadLibrary(L"ntdll.dll");
		_NtOpenKey = (lpNtOpenKey)GetProcAddress(module, "NtOpenKey");
		if (_NtOpenKey == NULL) {
			// This is a severe error
			OutputDebugString(_T("X0X0 ERROR: cannot find the address of NtOpenKey"));
			return 0;
		}
	}
	return _NtOpenKey(KeyHandle, DesiredAccess, ObjectAttributes);
}