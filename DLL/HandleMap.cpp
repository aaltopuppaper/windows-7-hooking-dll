#include "HandeMap.h"

// Add the Handle h associated to filename fname to the tracked handles. 
// The optional parameter marked_for_deletion is a flag that identifies a file
// which has been marked for deletion at a certain point. This flag will be considered 
// only when it's TRUE. Setting it to false makes no difference and won't update the internal state.
// If the specified handle is already available, it increases the associated counter.
// This method is thread safe.
void HandleMap::AddHandle(HANDLE h, TCHAR* fname, bool marked_for_deletion) {
	std::lock_guard<std::recursive_mutex> lock(m);
	std::map<HANDLE, FileHandleInfo>::iterator it;

	it = map.find(h);
	if (it == map.end()) {
		// This is the first time we see this handle.
		FileHandleInfo info;
		info.count = 1;
		info.marked_for_deletion = marked_for_deletion;
		info.name.assign(fname);
		map[h] = info;
	}
	else {
		// We need to increase the value of the reference count
		it->second.count += 1;
		// Only update the deletion flag if that is true.
		if (marked_for_deletion)
			it->second.marked_for_deletion = true;
	}
}

// This function raises the deletion flag regarding the provided handle. Once triggered, there is no other way to 
// reset this flag.
bool HandleMap::SetHandleMarkedForDeletion(HANDLE h) {
	std::lock_guard<std::recursive_mutex> lock(m);
	std::map<HANDLE, FileHandleInfo>::iterator it;

	it = map.find(h);
	if (it == map.end()) {
		return false;
	}
	else {
		it->second.marked_for_deletion = true;
		return true;
	}
}


// This method scans the map looking for a specified HANDLE. If a matching handle is found, the associated 
// filename is copied into the provided buffer and true is returned. If no match is found, the function returns false.
// Please note that the function will truncate the fname if the provided buffer is shorter that the file name length stored
// into the map. Better to use MAX_PATH as length of the buffer. The function also retrieves the marked_for_deletion flag
// then a pointer to a boolean field is passed. If such info is not needed, pass NULL.
// When the optional argument POP is set to true, the reference counter associated to the HANDLE is also decreased.
// If it hits 0, the item is definetely removed from the map.
bool HandleMap::GetPathFromHandle(HANDLE h, TCHAR* buff, SSIZE_T buff_size, bool* marked_for_deletion, bool pop) {
	std::lock_guard<std::recursive_mutex> lock(m);
	std::map<HANDLE, FileHandleInfo>::iterator it;

	it = map.find(h);
	if (it == map.end()) {
		// We did not find any value associated to this key. Just let the caller know.
		return false;
	}

	// We found a match, immediately copy the item into the buffer together with the deletion marker
	swprintf_s(buff, buff_size, TEXT("%s"), it->second.name.c_str());
	if (marked_for_deletion!=NULL)
		*marked_for_deletion = it->second.marked_for_deletion;

	if (pop) {
		// The caller specified the POP operation, we also want to decrease the referecen counter to this handle.
		it->second.count -= 1;

		// If it reaches 0, we remove it from the map.
		if (it->second.count == 0)
			map.erase(it);
	}

	return true;
}

