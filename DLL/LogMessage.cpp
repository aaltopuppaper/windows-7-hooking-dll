#include "LogMessage.h"
#pragma comment(lib, "crypt32.lib")

/* --------------- Base Class ---------------*/
// Constructor
LogMessage::LogMessage(wchar_t* id) {
	this->id.assign(id);
};
void LogMessage::SetPID(uint32_t pid) {
	this->pid = pid;
};
void LogMessage::SetTID(uint32_t tid) {
	this->tid = tid;
};

// This function serializes the message into json format and returns it in form of a wstring.
wstring LogMessage::ToJson() {
	// Use the following objects to write the json object.
	GenericStringBuffer< rapidjson::UTF16<> > s;
	Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>> writer(s);

	// First the id of message.
	writer.StartObject();
	writer.Key(LOG_MESSAGE_KEY_ID);
	writer.String(this->id.c_str());

	writer.Key(LOG_MESSAGE_KEY_ID_PID);
	writer.Int(this->pid);
	writer.Key(LOG_MESSAGE_KEY_ID_TID);
	writer.Int(this->tid);

	// Let the sub class write itsown specific fields
	this->WriteJson(writer);

	writer.EndObject();

	// Now return the string
	s.Flush();
	return std::wstring(s.GetString());
};

/* --------------- FileOpenMessage ---------------*/
// Constructor
FileOpenMessage::FileOpenMessage() :LogMessage(LOG_MESSAGE_KEY_ID_FILEOPENED) {
	this->exists = false;
	this->size = 0;
	this->path[0] = '\0';
	this->md5[0] = '\0';
	this->sha1[0] = '\0';
};

// Setters
void FileOpenMessage::SetPath(wchar_t* path) {wcscpy(this->path, path);};
void FileOpenMessage::SetExists(bool exists) {this->exists = true;};
void FileOpenMessage::SetSize(uint64_t size) { this->size = size; };
void FileOpenMessage::SetSHA1(wchar_t* sha1) { wcscpy(this->sha1, sha1); this->sha1[40] = '\0'; };
void FileOpenMessage::SetMD5(wchar_t* md5) { wcscpy(this->md5, md5); this->md5[32] = '\0'; };

void FileOpenMessage::WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer) {
	writer.Key(LOG_MESSAGE_KEY_ID_FILE_ORIGINAL_PATH);
	writer.String(this->path);
	
	writer.Key(LOG_MESSAGE_KEY_ID_FILE_SIZE);
	writer.Uint64(this->size);

	writer.Key(LOG_MESSAGE_KEY_ID_FILE_SHA1);
	writer.String(this->sha1);

	writer.Key(LOG_MESSAGE_KEY_ID_FILE_MD5);
	writer.String(this->md5);

	writer.Key(LOG_MESSAGE_KEY_ID_FILE_EXISTED);
	writer.Bool(this->exists);
};

/* --------------- FileCloseMessage ---------------*/
// Constructor
FileCloseMessage::FileCloseMessage() :LogMessage(LOG_MESSAGE_KEY_ID_FILECLOSED) {
	this->exists = false;
	this->size = 0;
	this->path[0] = '\0';
	this->md5[0] = '\0';
	this->sha1[0] = '\0';
};

// Setters
void FileCloseMessage::SetPath(wchar_t* path) { wcscpy(this->path, path); };
void FileCloseMessage::SetExists(bool exists) { this->exists = true; };
void FileCloseMessage::SetSize(uint64_t size) { this->size = size; };
void FileCloseMessage::SetSHA1(wchar_t* sha1) { wcscpy(this->sha1, sha1); this->sha1[40] = '\0'; };
void FileCloseMessage::SetMD5(wchar_t* md5) { wcscpy(this->md5, md5); this->md5[32] = '\0'; };
void FileCloseMessage::SetMarkedForDeletion(bool marked_for_deletion) { this->marked_for_deletion = marked_for_deletion; };
void FileCloseMessage::WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer) {
	writer.Key(LOG_MESSAGE_KEY_ID_FILE_ORIGINAL_PATH);
	writer.String(this->path);

	writer.Key(LOG_MESSAGE_KEY_ID_FILE_SIZE);
	writer.Uint64(this->size);

	writer.Key(LOG_MESSAGE_KEY_ID_FILE_SHA1);
	writer.String(this->sha1);

	writer.Key(LOG_MESSAGE_KEY_ID_FILE_MD5);
	writer.String(this->md5);

	writer.Key(LOG_MESSAGE_KEY_ID_FILE_EXISTED);
	writer.Bool(this->exists);
};

/* --------------- FileRenameMessage ---------------*/
// Constructor
FileRenameMessage::FileRenameMessage() :LogMessage(LOG_MESSAGE_KEY_ID_FILERENAMED) {
	this->o_path[0] = '\0';
	this->f_path[0] = '\0';
	this->size = 0;
	this->md5[0] = '\0';
	this->sha1[0] = '\0';
};

// Setters
void FileRenameMessage::SetOPath(wchar_t* path) { wcscpy(this->o_path, path); };
void FileRenameMessage::SetFPath(wchar_t* path) { wcscpy(this->f_path, path); };
void FileRenameMessage::SetSize(uint64_t size) { this->size = size; };
void FileRenameMessage::SetSHA1(wchar_t* sha1) { wcscpy(this->sha1, sha1); this->sha1[40] = '\0'; };
void FileRenameMessage::SetMD5(wchar_t* md5) { wcscpy(this->md5, md5); this->md5[32] = '\0'; };

void FileRenameMessage::WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer) {
	writer.Key(LOG_MESSAGE_KEY_ID_FILE_ORIGINAL_PATH);
	writer.String(this->o_path);

	writer.Key(LOG_MESSAGE_KEY_ID_FILE_FINAL_PATH);
	writer.String(this->o_path);

	writer.Key(LOG_MESSAGE_KEY_ID_FILE_SHA1);
	writer.String(this->sha1);

	writer.Key(LOG_MESSAGE_KEY_ID_FILE_MD5);
	writer.String(this->md5);

	writer.Key(LOG_MESSAGE_KEY_ID_FILE_SIZE);
	writer.Uint64(this->size);

};

/* --------------- FileDeleteMessage ---------------*/
// Constructor
FileDeleteMessage::FileDeleteMessage() :LogMessage(LOG_MESSAGE_KEY_ID_FILEDELETED) {
	this->path[0] = '\0';
	this->size = 0;
	this->md5[0] = '\0';
	this->sha1[0] = '\0';
	this->existed = false;
};

// Setters
void FileDeleteMessage::SetPath(wchar_t* path) { wcscpy(this->path,  path); };
void FileDeleteMessage::SetSize(uint64_t size) { this->size = size; };
void FileDeleteMessage::SetSHA1(wchar_t* sha1) { wcscpy(this->sha1, sha1); this->sha1[40] = '\0'; };
void FileDeleteMessage::SetMD5(wchar_t* md5) { wcscpy(this->md5, md5); this->md5[32] = '\0'; };
void FileDeleteMessage::SetExists(bool existed) { this->existed = existed; };

void FileDeleteMessage::WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer) {
	writer.Key(LOG_MESSAGE_KEY_ID_FILE_ORIGINAL_PATH);
	writer.String(this->path);

	writer.Key(LOG_MESSAGE_KEY_ID_FILE_SHA1);
	writer.String(this->sha1);

	writer.Key(LOG_MESSAGE_KEY_ID_FILE_MD5);
	writer.String(this->md5);

	writer.Key(LOG_MESSAGE_KEY_ID_FILE_SIZE);
	writer.Uint64(this->size);

};

/* --------------- RegistryNewKeyMessage ---------------*/ 
// Constructor
RegistryNewKeyMessage::RegistryNewKeyMessage():LogMessage(LOG_MESSAGE_KEY_ID_REGISTRY_KEY_CREATED) {
	this->path[0] = '\0';
};

// Setters
void RegistryNewKeyMessage::SetPath(wchar_t* path) { wcscpy(this->path, path); };

void RegistryNewKeyMessage::WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer) {
	writer.Key(LOG_MESSAGE_KEY_ID_REGISTRY_KEY_PATH);
	writer.String(this->path);
};

/* --------------- RegistryDeleteKeyMessage ---------------*/
// Constructor
RegistryDeleteKeyMessage::RegistryDeleteKeyMessage() :LogMessage(LOG_MESSAGE_KEY_ID_REGISTRY_KEY_DELETED) {
	this->path[0] = '\0';
};

// Setters
void RegistryDeleteKeyMessage::SetPath(wchar_t* path) { wcscpy(this->path, path); };

void RegistryDeleteKeyMessage::WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer) {
	writer.Key(LOG_MESSAGE_KEY_ID_REGISTRY_KEY_PATH);
	writer.String(this->path);
};

/* --------------- RegistryRenamedKeyMessage ---------------*/
// Constructor
RegistryRenamedKeyMessage::RegistryRenamedKeyMessage() :LogMessage(LOG_MESSAGE_KEY_ID_REGISTRY_KEY_RENAMED) {
	this->new_path[0] = '\0';
	this->old_path[0] = '\0';
};

// Setters
void RegistryRenamedKeyMessage::SetOldPath(wchar_t* path) { wcscpy(this->old_path, path); };
void RegistryRenamedKeyMessage::SetNewPath(wchar_t* path) { wcscpy(this->new_path, path); };

void RegistryRenamedKeyMessage::WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer) {
	writer.Key(LOG_MESSAGE_KEY_ID_REGISTRY_OLD);
	writer.String(this->old_path);

	writer.Key(LOG_MESSAGE_KEY_ID_REGISTRY_NEW);
	writer.String(this->new_path);
};

/* --------------- RegistryNewValueMessage ---------------*/
// Constructor
RegistryNewValueMessage::RegistryNewValueMessage() : LogMessage(LOG_MESSAGE_KEY_ID_REGISTRY_VALUE_CREATED) {
	this->key_path[0] = '\0';
	this->value_name.assign(L"");
	this->value_type = 0; // VALUETYPE_NONE
	this->data = NULL;
	this->data_len = 0;
};

// Setters
void RegistryNewValueMessage::SetKeyPath(wchar_t* path) { wcscpy(this->key_path, path); };
void RegistryNewValueMessage::SetValueName(wchar_t* value_name) { this->value_name.assign(value_name); };
void RegistryNewValueMessage::SetValueData(unsigned long value_type, void* data_ptr, unsigned long data_len) {
	if (this->data != NULL) {
		delete[] this->data;
	}

	this->data = new byte[data_len];
	memcpy(this->data, data_ptr, data_len);
	this->data_len = data_len;
	this->value_type = value_type;
	// TODO: check value type?
};

RegistryNewValueMessage::~RegistryNewValueMessage() {
	this->key_path[0] = '\0';
	this->value_name.assign(L"");
	this->value_type = 0; // VALUETYPE_NONE
	if (this->data != NULL) {
		delete[] this->data;
		this->data = NULL;
	}
	this->data_len = 0;
};

void RegistryNewValueMessage::WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer) {
	unsigned long counter = 0;
	int next_token_len = 0;
	DWORD dest;
	DWORD size;
	wchar_t* buff = NULL;

	writer.Key(LOG_MESSAGE_KEY_ID_REGISTRY_KEY_PATH);
	writer.String(this->key_path);

	writer.Key(LOG_MESSAGE_KEY_ID_REGISTRY_VALUE_NAME);
	writer.String(this->value_name.c_str());

	// Convert value types into string representation
	writer.Key(LOG_MESSAGE_KEY_ID_REGISTRY_VALUE_TYPE);
	switch (this->value_type) {
	case REG_LINK:
		writer.String(REGISTRY_VALUE_TYPE_LINK);
		break;
	case REG_RESOURCE_LIST:
		writer.String(REGISTRY_VALUE_TYPE_RESOURCE_LIST);
		break;
	case REG_RESOURCE_REQUIREMENTS_LIST:
		writer.String(REGISTRY_VALUE_TYPE_REQUIREMENTS_LIST);
		break;
	case REG_FULL_RESOURCE_DESCRIPTOR:
		writer.String(REGISTRY_VALUE_TYPE_RESOURCE_DESCRIPTOR);
		break;
	case REG_DWORD_BIG_ENDIAN:
		writer.String(REGISTRY_VALUE_TYPE_DWORD_BIG_ENDIAN);
		break;
	case REG_DWORD:
		writer.String(REGISTRY_VALUE_TYPE_DWORD);
		break;
	case REG_QWORD:
		writer.String(REGISTRY_VALUE_TYPE_QWORD);
		break;
	case REG_SZ:
		writer.String(REGISTRY_VALUE_TYPE_SZ);
		break;
	case REG_EXPAND_SZ:
		writer.String(REGISTRY_VALUE_TYPE_EXPAND_SZ);
		break;
	case REG_MULTI_SZ:
		writer.String(REGISTRY_VALUE_TYPE_MULTI_SZ);
		break;
	case REG_BINARY:
		writer.String(REGISTRY_VALUE_TYPE_BINARY);
		break;
	case REG_NONE:
	default:
		writer.String(REGISTRY_VALUE_TYPE_NONE);
		break;
	};
	
	// Now write data in a covenient format the data payload
	writer.Key(LOG_MESSAGE_KEY_ID_REGISTRY_VALUE_DATA);
	switch (this->value_type) {
	case REG_MULTI_SZ:
		// The buffer contains a sequence of UNICODE strings terminated by an empty string.
		// Let's treat the buffer as a sequence of wchar_t
		writer.StartArray();
		while (counter*sizeof(wchar_t) < this->data_len) {
			next_token_len = wcslen(((wchar_t*)this->data) + counter);
			if (next_token_len == 0)
				// Empty string!
				break;
			writer.String(((wchar_t*)this->data) + counter);
			counter += next_token_len + 1; //+1 for the terminator which is not counted by wcslen.
		}
		writer.EndArray();
		break;
	case REG_SZ:
	case REG_EXPAND_SZ:
	case REG_LINK:
		// Is a null terminated string containing the path of the linked reg-key
		writer.String((wchar_t*)this->data);
		break;
	case REG_DWORD_BIG_ENDIAN:
		// Invert endianess and write the corresponding number
		swab((char*)this->data, (char*)&dest, sizeof(DWORD));
		writer.Uint(*((DWORD*)dest));
		break;
	case REG_DWORD:
		// Just an unsigned 32bit value
		writer.Uint(*((DWORD*)this->data));
		break;
	case REG_QWORD:
		// Just an unsigned 32bit value
		writer.Uint(*((unsigned __int64*)this->data));
		break;
	case REG_NONE:
	case REG_RESOURCE_LIST:
	case REG_RESOURCE_REQUIREMENTS_LIST:
	case REG_FULL_RESOURCE_DESCRIPTOR:
	case REG_BINARY:
	default:
		// Convert in Base64 and write
		// First let's calculate the needed size
		if (CryptBinaryToString(this->data, this->data_len, CRYPT_STRING_BASE64 | CRYPT_STRING_NOCRLF, NULL, &size) == 0) {
			// Failure
			writer.Null();
			break;
		};

		// Allocate needed buffer and perform the logging
		buff = new wchar_t[size];
		if (CryptBinaryToString(this->data, this->data_len, CRYPT_STRING_BASE64 | CRYPT_STRING_NOCRLF, buff, &size) == 0) {
			// Failure
			writer.Null();
			delete[] buff;
			break;
		};
		writer.String(buff);

		delete[] buff;
		break;
	};
};

/* --------------- RegistryModifiedValueMessage ---------------*/
// Constructor
RegistryModifiedValueMessage::RegistryModifiedValueMessage() : LogMessage(LOG_MESSAGE_KEY_ID_REGISTRY_VALUE_EDITED) {
	this->key_path[0] = '\0';
	this->old_data.value_name.assign(L"");
	this->new_data.value_name.assign(L"");

	this->old_data.value_type = 0; // VALUETYPE_NONE
	this->new_data.value_type = 0;

	this->old_data.data = NULL;
	this->new_data.data = NULL;

	this->old_data.data_len = 0;
	this->new_data.data_len = 0;
};

// Setters
void RegistryModifiedValueMessage::SetKeyPath(wchar_t* path) { wcscpy(this->key_path, path); };
void RegistryModifiedValueMessage::SetOldValueName(wchar_t* value_name) { this->old_data.value_name.assign(value_name); };
void RegistryModifiedValueMessage::SetNewValueName(wchar_t* value_name) { this->new_data.value_name.assign(value_name); };
void RegistryModifiedValueMessage::SetOldValueData(unsigned long value_type, void* data_ptr, unsigned long data_len) {
	if (this->old_data.data != NULL) {
		delete[] this->old_data.data;
	}

	this->old_data.data = new byte[data_len];
	memcpy(this->old_data.data, data_ptr, data_len);
	this->old_data.data_len = data_len;
	this->old_data.value_type = value_type;
	// TODO: check value type?
};

void RegistryModifiedValueMessage::SetNewValueData(unsigned long value_type, void* data_ptr, unsigned long data_len) {
	if (this->new_data.data != NULL) {
		delete[] this->new_data.data;
	}

	this->new_data.data = new byte[data_len];
	memcpy(this->new_data.data, data_ptr, data_len);
	this->new_data.data_len = data_len;
	this->new_data.value_type = value_type;
	// TODO: check value type?
};


RegistryModifiedValueMessage::~RegistryModifiedValueMessage() {
	this->key_path[0] = '\0';

	this->old_data.value_name.assign(L"");
	this->new_data.value_name.assign(L"");
	
	this->old_data.value_type = 0; // VALUETYPE_NONE
	this->new_data.value_type = 0; // VALUETYPE_NONE

	if (this->old_data.data != NULL) {
		delete[] this->old_data.data;
		this->old_data.data = NULL;
	}
	this->old_data.data_len = 0;

	if (this->new_data.data != NULL) {
		delete[] this->new_data.data;
		this->new_data.data = NULL;
	}
	this->new_data.data_len = 0;
};

void RegistryModifiedValueMessage::WriteJson(Writer<GenericStringBuffer<rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer) {
	writer.Key(LOG_MESSAGE_KEY_ID_REGISTRY_KEY_PATH);
	writer.String(this->key_path);

	writer.Key(LOG_MESSAGE_KEY_ID_REGISTRY_OLD);
	writer.StartObject();
	WriteDataObject(writer, this->old_data);
	writer.EndObject();

	writer.Key(LOG_MESSAGE_KEY_ID_REGISTRY_NEW);
	writer.StartObject();
	WriteDataObject(writer, this->new_data);
	writer.EndObject();	
};

void RegistryModifiedValueMessage::WriteDataObject(Writer<GenericStringBuffer<rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer, KeyValueName& val) {
	unsigned long counter = 0;
	int next_token_len = 0;
	DWORD dest;
	DWORD size;
	wchar_t* buff = NULL;

	writer.Key(LOG_MESSAGE_KEY_ID_REGISTRY_VALUE_NAME);
	writer.String(val.value_name.c_str());

	// Convert value types into string representation
	writer.Key(LOG_MESSAGE_KEY_ID_REGISTRY_VALUE_TYPE);
	switch (val.value_type) {
	case REG_LINK:
		writer.String(REGISTRY_VALUE_TYPE_LINK);
		break;
	case REG_RESOURCE_LIST:
		writer.String(REGISTRY_VALUE_TYPE_RESOURCE_LIST);
		break;
	case REG_RESOURCE_REQUIREMENTS_LIST:
		writer.String(REGISTRY_VALUE_TYPE_REQUIREMENTS_LIST);
		break;
	case REG_FULL_RESOURCE_DESCRIPTOR:
		writer.String(REGISTRY_VALUE_TYPE_RESOURCE_DESCRIPTOR);
		break;
	case REG_DWORD_BIG_ENDIAN:
		writer.String(REGISTRY_VALUE_TYPE_DWORD_BIG_ENDIAN);
		break;
	case REG_DWORD:
		writer.String(REGISTRY_VALUE_TYPE_DWORD);
		break;
	case REG_QWORD:
		writer.String(REGISTRY_VALUE_TYPE_QWORD);
		break;
	case REG_SZ:
		writer.String(REGISTRY_VALUE_TYPE_SZ);
		break;
	case REG_EXPAND_SZ:
		writer.String(REGISTRY_VALUE_TYPE_EXPAND_SZ);
		break;
	case REG_MULTI_SZ:
		writer.String(REGISTRY_VALUE_TYPE_MULTI_SZ);
		break;
	case REG_BINARY:
		writer.String(REGISTRY_VALUE_TYPE_BINARY);
		break;
	case REG_NONE:
	default:
		writer.String(REGISTRY_VALUE_TYPE_NONE);
		break;
	};

	// Now write data in a covenient format the data payload
	writer.Key(LOG_MESSAGE_KEY_ID_REGISTRY_VALUE_DATA);
	switch (val.value_type) {
	case REG_MULTI_SZ:
		// The buffer contains a sequence of UNICODE strings terminated by an empty string.
		// Let's treat the buffer as a sequence of wchar_t
		writer.StartArray();
		while (counter*sizeof(wchar_t) < val.data_len) {
			next_token_len = wcslen(((wchar_t*)val.data) + counter);
			if (next_token_len == 0)
				// Empty string!
				break;
			writer.String(((wchar_t*)val.data) + counter);
			counter += next_token_len + 1; //+1 for the terminator which is not counted by wcslen.
		}
		writer.EndArray();
		break;
	case REG_SZ:
	case REG_EXPAND_SZ:
	case REG_LINK:
		// Is a null terminated string containing the path of the linked reg-key
		writer.String((wchar_t*)val.data);
		break;
	case REG_DWORD_BIG_ENDIAN:
		// Invert endianess and write the corresponding number
		swab((char*)val.data, (char*)&dest, sizeof(DWORD));
		writer.Uint(*((DWORD*)dest));
		break;
	case REG_DWORD:
		// Just an unsigned 32bit value
		writer.Uint(*((DWORD*)val.data));
		break;
	case REG_QWORD:
		// Just an unsigned 32bit value
		writer.Uint(*((unsigned __int64*)val.data));
		break;
	case REG_NONE:
	case REG_RESOURCE_LIST:
	case REG_RESOURCE_REQUIREMENTS_LIST:
	case REG_FULL_RESOURCE_DESCRIPTOR:
	case REG_BINARY:
	default:
		// Convert in Base64 and write
		// First let's calculate the needed size
		if (CryptBinaryToString(val.data, val.data_len, CRYPT_STRING_BASE64 | CRYPT_STRING_NOCRLF, NULL, &size) == 0) {
			// Failure
			writer.Null();
			break;
		};

		// Allocate needed buffer and perform the logging
		buff = new wchar_t[size];
		if (CryptBinaryToString(val.data, val.data_len, CRYPT_STRING_BASE64 | CRYPT_STRING_NOCRLF, buff, &size) == 0) {
			// Failure
			writer.Null();
			delete[] buff;
			break;
		};
		writer.String(buff);

		delete[] buff;
		break;
	};
}

/* --------------- RegistryDeletedValueMessage ---------------*/
// Constructor
RegistryDeletedValueMessage::RegistryDeletedValueMessage() : LogMessage(LOG_MESSAGE_KEY_ID_REGISTRY_VALUE_DELETED) {
	this->key_path[0] = '\0';
	this->old_value_name.assign(L"");
	this->old_value_type = 0; // VALUETYPE_NONE
	this->old_data = NULL;
	this->old_data_len = 0;
};

// Setters
void RegistryDeletedValueMessage::SetOldKeyPath(wchar_t* path) { wcscpy(this->key_path, path); };
void RegistryDeletedValueMessage::SetOldValueName(wchar_t* value_name) { this->old_value_name.assign(value_name); };
void RegistryDeletedValueMessage::SetOldValueData(unsigned long value_type, void* data_ptr, unsigned long data_len) {
	if (this->old_data != NULL) {
		delete[] this->old_data;
	}

	this->old_data = new byte[data_len];
	memcpy(this->old_data, data_ptr, data_len);
	this->old_data_len = data_len;
	this->old_value_type = value_type;
	// TODO: check value type?
};

RegistryDeletedValueMessage::~RegistryDeletedValueMessage() {
	this->key_path[0] = '\0';
	this->old_value_name.assign(L"");
	this->old_value_type = 0; // VALUETYPE_NONE
	if (this->old_data != NULL) {
		delete[] this->old_data;
		this->old_data = NULL;
	}
	this->old_data_len = 0;
};

void RegistryDeletedValueMessage::WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer) {
	unsigned long counter = 0;
	int next_token_len = 0;
	DWORD dest;
	DWORD size;
	wchar_t* buff = NULL;

	writer.Key(LOG_MESSAGE_KEY_ID_REGISTRY_KEY_PATH);
	writer.String(this->key_path);

	writer.Key(LOG_MESSAGE_KEY_ID_REGISTRY_VALUE_NAME);
	writer.String(this->old_value_name.c_str());

	// Convert value types into string representation
	writer.Key(LOG_MESSAGE_KEY_ID_REGISTRY_VALUE_TYPE);
	switch (this->old_value_type) {
	case REG_LINK:
		writer.String(REGISTRY_VALUE_TYPE_LINK);
		break;
	case REG_RESOURCE_LIST:
		writer.String(REGISTRY_VALUE_TYPE_RESOURCE_LIST);
		break;
	case REG_RESOURCE_REQUIREMENTS_LIST:
		writer.String(REGISTRY_VALUE_TYPE_REQUIREMENTS_LIST);
		break;
	case REG_FULL_RESOURCE_DESCRIPTOR:
		writer.String(REGISTRY_VALUE_TYPE_RESOURCE_DESCRIPTOR);
		break;
	case REG_DWORD_BIG_ENDIAN:
		writer.String(REGISTRY_VALUE_TYPE_DWORD_BIG_ENDIAN);
		break;
	case REG_DWORD:
		writer.String(REGISTRY_VALUE_TYPE_DWORD);
		break;
	case REG_QWORD:
		writer.String(REGISTRY_VALUE_TYPE_QWORD);
		break;
	case REG_SZ:
		writer.String(REGISTRY_VALUE_TYPE_SZ);
		break;
	case REG_EXPAND_SZ:
		writer.String(REGISTRY_VALUE_TYPE_EXPAND_SZ);
		break;
	case REG_MULTI_SZ:
		writer.String(REGISTRY_VALUE_TYPE_MULTI_SZ);
		break;
	case REG_BINARY:
		writer.String(REGISTRY_VALUE_TYPE_BINARY);
		break;
	case REG_NONE:
	default:
		writer.String(REGISTRY_VALUE_TYPE_NONE);
		break;
	};

	// Now write data in a covenient format the data payload
	writer.Key(LOG_MESSAGE_KEY_ID_REGISTRY_VALUE_DATA);
	switch (this->old_value_type) {
	case REG_MULTI_SZ:
		// The buffer contains a sequence of UNICODE strings terminated by an empty string.
		// Let's treat the buffer as a sequence of wchar_t
		writer.StartArray();
		while (counter*sizeof(wchar_t) < this->old_data_len) {
			next_token_len = wcslen(((wchar_t*)this->old_data) + counter);
			if (next_token_len == 0)
				// Empty string!
				break;
			writer.String(((wchar_t*)this->old_data) + counter);
			counter += next_token_len + 1; //+1 for the terminator which is not counted by wcslen.
		}
		writer.EndArray();
		break;
	case REG_SZ:
	case REG_EXPAND_SZ:
	case REG_LINK:
		// Is a null terminated string containing the path of the linked reg-key
		writer.String((wchar_t*)this->old_data);
		break;
	case REG_DWORD_BIG_ENDIAN:
		// Invert endianess and write the corresponding number
		swab((char*)this->old_data, (char*)&dest, sizeof(DWORD));
		writer.Uint(*((DWORD*)dest));
		break;
	case REG_DWORD:
		// Just an unsigned 32bit value
		writer.Uint(*((DWORD*)this->old_data));
		break;
	case REG_QWORD:
		// Just an unsigned 32bit value
		writer.Uint(*((unsigned __int64*)this->old_data));
		break;
	case REG_NONE:
	case REG_RESOURCE_LIST:
	case REG_RESOURCE_REQUIREMENTS_LIST:
	case REG_FULL_RESOURCE_DESCRIPTOR:
	case REG_BINARY:
	default:
		// Convert in Base64 and write
		// First let's calculate the needed size
		if (CryptBinaryToString(this->old_data, this->old_data_len, CRYPT_STRING_BASE64 | CRYPT_STRING_NOCRLF, NULL, &size) == 0) {
			// Failure
			writer.Null();
			break;
		};

		// Allocate needed buffer and perform the logging
		buff = new wchar_t[size];
		if (CryptBinaryToString(this->old_data, this->old_data_len, CRYPT_STRING_BASE64 | CRYPT_STRING_NOCRLF, buff, &size) == 0) {
			// Failure
			writer.Null();
			delete[] buff;
			break;
		};
		writer.String(buff);

		delete[] buff;
		break;
	};
};