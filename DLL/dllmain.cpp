/*
	-- Hooking DLL -- Revision 2.0alpha
	Author: Alberto Geniola, <albertogeniola@gmail.com>, 17/05/2017

	// TODO! We should take into account that newer windows versions might use longer paths. So we should redefine MAX_PATH from 260 up to 32,767 or use some dynamic allocation
	// all over the code in order to address this issue.

	TODO
	Appunti: A quanto pare, la chiamata di NtClose non implica il flushing dei dati sul disco. Questo significa che il calcolo dell'hash
	deve avvenire ANCHE dopo che il processo � stato terminato. Sar� quindi cura dell'AGENT di tenere traccia di tutti i file aperti/modificati
	e di eseguire l'ultimo check di esistenza e di hashing dei file rimasti in "pending".
	Nota anche che l'eliminazione dei file pu� avvenire tramite chiamate ad API diverse da NtDeleteFIle. Per esempio si pu� chiamare 
	Infatti, MSDN recita:
		Callers of NtClose should not assume that this routine automatically waits for all I/O to complete prior to returning.
	TODO

	This is the DLL to be injected into processes that we want to monitor.
	In order to work correctly, it must be compiled in both 32 and 64 bit versions. The injector will then detect the type of the target process
	and choose whether to inject the 32 bit version or the 64 bit one.

	We do monitor both FileSystem APIs and Registry APIs. 

	Note about FS access handling.
	We do intercept NtOpenFile NtCreateFile and NtClose. When opening or creating a file, we check if the caller is running with write permissions. In case it is not, 
	we simply invoke the original version of the API, thus not logging anything. If the caller has requested write or execute permissions on the file, we check if the
	targeted file exists. If so, we also calculate an HASH of the file before any access happens. Then we invoke the original function and if it succeeds, we notify the
	Agent about a successful NtOpen/Create operation on a specific file. In this message we also provide the information if the file existed and, possibly, its previous
	hash (if file already existed). When the called invokes the NtClose, we check if the call is about a file handle, and if so, we calculate its hash and notify the
	Agent. In such way, the Agent will always have an Initial state and a Final state for every file that has been accessed with WRITE/EXECUTE permissions.

	Note About file deletion.
	Deleting a file on Windows NT can ba accomplished in a number of manners. There are some explicit APIs such as NtDeleteFile that do that. However, (starting from windows 8,
	there also are some other cases in which the file can be delete. An example is when calling CreateFile/CreateFileEx with the FILE_DELETE_ON_CLOSE flag set. This flag tells 
	the OS to remove the file as soon as it gets closed. However, there is no insurance regarding such operation. As stated here http://www.osronline.com/showThread.cfm?link=142365
	there are evidences that multiple file opening calls with SHARED_DELETE disposition may lead to tricky situations. In particular, multiple handles to the file might get 
	into race conditions and the final result of the deletion is somehow hard to predict. In fact it may fail for a number of reasons, therefore it is very hard to check if
	all possible cases from within this DLL. 
	In order to solve such problem, we apply a simple solution. We track every handle opened with write/delete permissions in NtCreateFile and NtOpenFile in a map. Then, when
	intercepting any NtClose operation regarding such files, we check if the handle has any FILE_DELETE_ON_CLOSE flag set. If that is the case, we notify the Agent, so that it can
	eventually check if the file was successfully deleted or not.

	When we intercept on the the following cases, we assume the file is GOING-TO-BE-DELETED. So we notify the Agent which will eventually check if the file was effectively removed
	after the execution of the process is terminated. By doing so we can be 100% sure if the file survived or not the installation, while reducing the overhead of checking for the 
	existance of tons of files.

	Note about registry access handling.
	Registry access is logged differently in case of the registry. In fact, the DLL will hook the most known Nt-Level APIs that allow registry manipulation. Everytime one of them is 
	intercepted, we DLL retrieves some more info about the event and sends it back to the Agent only if the event succeeded. 

	Note: Recursive injection.
	TODO

	Note: How to add/remove hooks.
	In order to hook a specific API, some steps are needed. First of all, it is necessary to retrieve the handle to the module containing the
	API to be intercepted. For NTDLL APIs, we would need the ntdll.dll module. For user32, we'd need user32.dll. To retrieve an handle to such
	modules (if not obtained yet), use NktHookLibHelpers::GetModuleBaseAddress("<dll_name>").
	Then, define the signature of the hooking functions in the dllmain.h resource file. For each API to be hooked, you'll need:

	1. A typedef of the function pointer, matching the signature of the API to be hooked.
	2. A static declaration of the function we want to hook (which will be later on implemented in this file)
	3. A struct, which will used by DeviareInProc for storing the original API address (without trampoline)
	
	Once everything has been declared, in DLL_PROCESS_ATTACH handler you have to call cHookMgr.Hook() and pass the address of the function to be 
	hooked and all the previously defined variables (refer to NtOpenFile as example).
	
	Rememebr to unhook the functions in DLL_PROCESS_DETACH.

	// Future implementations
	-> Make the DLL reside in memory and implement custom LoadLibrary (private function LdrpLoadLibrary). This should prevent easy detection of DLL injection: https://www.joachim-bauch.de/tutorials/loading-a-dll-from-memory/
	   Also read this: http://blog.aaronballman.com/2011/08/what-happens-when-you-load-a-library/
	-> 
*/

#include "stdafx.h"
#include "dllmain.h"
#include "utils.h"
#include "HandeMap.h"

#define FILE_DELETE_ON_CLOSE 0x00001000
//#include <wininternl.h>
//-----------------------------------------------------------

CNktHookLib cHookMgr;
LPVOID fnOrigFunc;
HINSTANCE hNtDll, hUser32;
DWORD dwOsErr;

// This map is a thread safe map implementation used to take track of handles with write access that have been
// somehow hooked during NtOperations.
HandleMap handles;

extern "C" BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		// Uncomment the following to have a change to attach a debugger at runtime.
		MessageBox(NULL, L"Waiting...", L"ATTACH DEBUGGER", MB_OK);
		Log(L"Loading Injected DLL...");
		cHookMgr.SetEnableDebugOutput(TRUE);

		// NTDLL
		hNtDll = NktHookLibHelpers::GetModuleBaseAddress(L"ntdll.dll");
		if (hNtDll == NULL) {
			Log(L"Error: Cannot get handle of ntdll.dll", true);
			return 0;
		}

		// Retrieve NtFlushBuffersFile pointer
		NtFlushBuffersFile = (lpfnNtFlushBuffersFile)NktHookLibHelpers::GetProcedureAddress(hNtDll, "NtFlushBuffersFile");
		if (NtFlushBuffersFile == NULL) {
			Log(L"Error: Cannot get address of NtFlushBuffersFile", true);
			return 0;
		}

		// Retrieve NtQueryInformationFile pointer
		NtQueryInformationFile = (lpfnNtQueryInformationFile)NktHookLibHelpers::GetProcedureAddress(hNtDll, "NtQueryInformationFile");
		if (NtQueryInformationFile == NULL) {
			Log(L"Error: Cannot get address of NtQueryInformationFile", true);
			return 0;
		}

		#pragma region ----------- NtClose Hook -----------

		fnOrigFunc = NktHookLibHelpers::GetProcedureAddress(hNtDll, "NtClose");
		if (fnOrigFunc == NULL) {
			Log(L"Error: Cannot get address of NtClose", true);
			return 0;
		}

		dwOsErr = cHookMgr.Hook(&(sNtClose_Hook.nHookId), (LPVOID*)&(sNtClose_Hook.fnNtClose),
			fnOrigFunc, Hooked_NtClose,
			// We want to disallow reentrancy, so that only the first Hooked function will trigger the logging procedures.
			NKTHOOKLIB_DisallowReentrancy
		);
		#pragma endregion

		#pragma region ----------- NtCreateFile Hook -----------
		fnOrigFunc = NktHookLibHelpers::GetProcedureAddress(hNtDll, "NtCreateFile");
		if (fnOrigFunc == NULL) {
			Log(L"Error: Cannot get address of NtCreateFile", true);
			return 0;
		}

		dwOsErr = cHookMgr.Hook(&(sNtCreateFile_Hook.nHookId), (LPVOID*)&(sNtCreateFile_Hook.fnNtCreateFile),
			fnOrigFunc, Hooked_NtCreateFile,
			// We want to disallow reentrancy, so that only the first Hooked function will trigger the logging procedures.
			NKTHOOKLIB_DisallowReentrancy
		);
		#pragma endregion

		#pragma region ----------- NtOpenFile Hook -----------
		fnOrigFunc = NktHookLibHelpers::GetProcedureAddress(hNtDll, "NtOpenFile");
		if (fnOrigFunc == NULL) {
			Log(L"Error: Cannot get address of NtOpenFile", true);
			return 0;
		}

		dwOsErr = cHookMgr.Hook(&(sNtOpenFile_Hook.nHookId), (LPVOID*)&(sNtOpenFile_Hook.fnNtOpenFile),
			fnOrigFunc, Hooked_NtOpenFile,
			// We want to disallow reentrancy, so that only the first Hooked function will trigger the logging procedures.
			NKTHOOKLIB_DisallowReentrancy
			);
		#pragma endregion

		#pragma region ----------- NtDeleteFile Hook -----------
		fnOrigFunc = NktHookLibHelpers::GetProcedureAddress(hNtDll, "NtDeleteFile");
		if (fnOrigFunc == NULL) {
			Log(L"Error: Cannot get address of NtDeleteFile", true);
			return 0;
		}

		dwOsErr = cHookMgr.Hook(&(sNtDeleteFile_Hook.nHookId), (LPVOID*)&(sNtDeleteFile_Hook.fnNtDeleteFile),
			fnOrigFunc, Hooked_NtDeleteFile,
			// We want to disallow reentrancy, so that only the first Hooked function will trigger the logging procedures.
			NKTHOOKLIB_DisallowReentrancy
		);
		#pragma endregion

		#pragma region ----------- NtSetInformationFile -----------
		fnOrigFunc = NktHookLibHelpers::GetProcedureAddress(hNtDll, "NtSetInformationFile");
		if (fnOrigFunc == NULL) {
			Log(L"Error: Cannot get address of NtSetInformationFile", true);
			return 0;
		}

		dwOsErr = cHookMgr.Hook(&(sNtSetInformationFile_Hook.nHookId), (LPVOID*)&(sNtSetInformationFile_Hook.fnNtSetInformationFile),
			fnOrigFunc, Hooked_NtSetInformationFile,
			// We want to disallow reentrancy, so that only the first Hooked function will trigger the logging procedures.
			NKTHOOKLIB_DisallowReentrancy
		);
		#pragma endregion

		#pragma region ----------- NtCreateKey -----------
		fnOrigFunc = NktHookLibHelpers::GetProcedureAddress(hNtDll, "NtCreateKey");
		if (fnOrigFunc == NULL) {
			Log(L"Error: Cannot get address of NtCreateKey", true);
			return 0;
		}

		dwOsErr = cHookMgr.Hook(&(sNtCreateKey_Hook.nHookId), (LPVOID*)&(sNtCreateKey_Hook.fnNtCreateKey),
			fnOrigFunc, Hooked_NtCreateKey,
			// We want to disallow reentrancy, so that only the first Hooked function will trigger the logging procedures.
			NKTHOOKLIB_DisallowReentrancy
			);
		#pragma endregion

		#pragma region ----------- NtDeleteKey -----------
		fnOrigFunc = NktHookLibHelpers::GetProcedureAddress(hNtDll, "NtDeleteKey");
		if (fnOrigFunc == NULL) {
			Log(L"Error: Cannot get address of NtDeleteKey", true);
			return 0;
		}

		dwOsErr = cHookMgr.Hook(&(sNtDeleteKey_Hook.nHookId), (LPVOID*)&(sNtDeleteKey_Hook.fnNtDeleteKey),
			fnOrigFunc, Hooked_NtDeleteKey,
			// We want to disallow reentrancy, so that only the first Hooked function will trigger the logging procedures.
			NKTHOOKLIB_DisallowReentrancy
			);
		#pragma endregion

		#pragma region ----------- NtRenameKey -----------
		fnOrigFunc = NktHookLibHelpers::GetProcedureAddress(hNtDll, "NtRenameKey");
		if (fnOrigFunc == NULL) {
			Log(L"Error: Cannot get address of NtRenameKey", true);
			return 0;
		}

		dwOsErr = cHookMgr.Hook(&(sNtRenameKey_Hook.nHookId), (LPVOID*)&(sNtRenameKey_Hook.fnNtRenameKey),
			fnOrigFunc, Hooked_NtRenameKey,
			// We want to disallow reentrancy, so that only the first Hooked function will trigger the logging procedures.
			NKTHOOKLIB_DisallowReentrancy
			);
		#pragma endregion

		#pragma region ----------- NtSetValueKey -----------
		fnOrigFunc = NktHookLibHelpers::GetProcedureAddress(hNtDll, "NtSetValueKey");
		if (fnOrigFunc == NULL) {
			Log(L"Error: Cannot get address of NtDeleteKey", true);
			return 0;
		}

		dwOsErr = cHookMgr.Hook(&(sNtsetValueKey_Hook.nHookId), (LPVOID*)&(sNtsetValueKey_Hook.fnNtsetValueKey),
			fnOrigFunc, Hooked_NtsetValueKey,
			// We want to disallow reentrancy, so that only the first Hooked function will trigger the logging procedures.
			NKTHOOKLIB_DisallowReentrancy
			);
		#pragma endregion

		#pragma region ----------- NtDeleteValueKey -----------
		fnOrigFunc = NktHookLibHelpers::GetProcedureAddress(hNtDll, "NtDeleteValueKey");
		if (fnOrigFunc == NULL) {
			Log(L"Error: Cannot get address of NtDeleteValueKey", true);
			return 0;
		}

		dwOsErr = cHookMgr.Hook(&(sNtDeleteValueKey_Hook.nHookId), (LPVOID*)&(sNtDeleteValueKey_Hook.fnNtDeleteValueKey),
			fnOrigFunc, Hooked_NtDeleteValueKey,
			// We want to disallow reentrancy, so that only the first Hooked function will trigger the logging procedures.
			NKTHOOKLIB_DisallowReentrancy
			);
		#pragma endregion

		// User32
		hUser32 = NktHookLibHelpers::GetModuleBaseAddress(L"C:\\Windows\\SysWOW64\\user32.dll");
		if (hNtDll == NULL) {
			Log(L"Error: Cannot get handle of user32.dll", true);
			return 0;
		}

		Log(L"DLL LOADED successfully.");
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		// Unload hooks.
		Log(L"UNLOADING DLL...");
		dwOsErr = cHookMgr.Unhook(sNtCreateFile_Hook.nHookId);
		dwOsErr = cHookMgr.Unhook(sNtOpenFile_Hook.nHookId);
		dwOsErr = cHookMgr.Unhook(sNtDeleteFile_Hook.nHookId);
		dwOsErr = cHookMgr.Unhook(sNtClose_Hook.nHookId);
		dwOsErr = cHookMgr.Unhook(sNtSetInformationFile_Hook.nHookId);
		dwOsErr = cHookMgr.Unhook(sNtCreateKey_Hook.nHookId);
		dwOsErr = cHookMgr.Unhook(sNtsetValueKey_Hook.nHookId);
		dwOsErr = cHookMgr.Unhook(sNtDeleteValueKey_Hook.nHookId);
		dwOsErr = cHookMgr.Unhook(sNtRenameKey_Hook.nHookId);
		Log(L"DLL unloaded successfully.");
		break;
	}
	return TRUE;
}

// This function will notify the Agent about an event, which is passes as Argument.
// The function will block until the event has been correctly notified to the agent.
// This gives some time to the agent to perfom any additional check just after the event is raised.
void NotifyAgent(LogMessage& m) {
	std::wstring msg;
	DWORD pid = GetCurrentProcessId();
	DWORD tid = GetCurrentThreadId();

	m.SetPID(pid);
	m.SetTID(tid);

	msg = m.ToJson();

	// TODO: remove this
#ifdef _DEBUG
	Log(msg.c_str());
#endif
	// TODO: implement message sending to the client.

};

#pragma region NtClose
static int WINAPI Hooked_NtClose(
	__in HANDLE FileHandle
) {
	TCHAR fpath[MAX_PATH];
	//TCHAR buffer[200 + MAX_PATH];
	bool marked_for_deletion = false;

	// We are interested in calculating hashes of closed files that were opened with Write Access.
	// We've stored such Handles in a specific table for faster lookups.
	// When the item is of our interest, we calculate the hash of the file and communicate that to the GuestAgent. 
	
	if(!handles.GetPathFromHandle(FileHandle, fpath, MAX_PATH, &marked_for_deletion, true))
	//if (!GetFileNameFromHandle(FileHandle, fpath, MAX_PATH))
		// This is not a file we are interested into.
		return sNtClose_Hook.fnNtClose(FileHandle);
	else {
		FileHashes hashes;
		IO_STATUS_BLOCK sb;
		ULONG create_options_flag = 0;
		
		// Before closing the file, attempt to force the OS to flush data, so we can calculate an updated hash of the file
		// Note: I'm not sure this will effectively flush the buffers. This is just an attempt to force the os to do so.
		NtFlushBuffersFile(FileHandle, &sb);

		// Now calculate the file hash once again
		calc_file_hashes(fpath, &hashes);

		//swprintf(buffer, _T("Hooked_NtClose on file %s -> SHA1: %s, MD5: %s. DELETE_ON_CLOSE: %d"), fpath, hashes.sha1_hex, hashes.md5_hex, marked_for_deletion ? 1 : 0);
		//Log(buffer);

		FileCloseMessage m = FileCloseMessage();
		m.SetMD5(hashes.md5_hex);
		m.SetSHA1(hashes.sha1_hex);
		m.SetPath(fpath);
		m.SetSize(hashes.size);
		m.SetExists(hashes.file_existed);
		m.SetMarkedForDeletion(marked_for_deletion);

		// Let the agent know about this. This will block until the notification is received by the Agent.
		NotifyAgent(m);

		return sNtClose_Hook.fnNtClose(FileHandle);
	} 
}
#pragma endregion

// File functions
#pragma region Hooking NtOpenFile
static int WINAPI Hooked_NtOpenFile(
	__out PHANDLE            FileHandle,
	__in  ACCESS_MASK        DesiredAccess,
	__in  POBJECT_ATTRIBUTES ObjectAttributes,
	__out PIO_STATUS_BLOCK   IoStatusBlock,
	__in  ULONG              ShareAccess,
	__in  ULONG              OpenOptions
	)
{
	// Skip what we are not interested into: some special file (PIPEs, etc) and any kind of access performed without write/execute perms.
	if (is_directory_objattr(ObjectAttributes) == 0 && is_ignored_file_unicode(ObjectAttributes->ObjectName->Buffer,
		ObjectAttributes->ObjectName->Length / sizeof(wchar_t)) == 0 && (access_mask_write_perm(DesiredAccess) || access_mask_execute_perm(DesiredAccess))) {

		wchar_t fname[MAX_PATH_PLUS_TOLERANCE]; 
		uint32_t length;
		//wchar_t buffer[256 + MAX_PATH_PLUS_TOLERANCE];
		HANDLE hfile;
		IO_STATUS_BLOCK sb;
		bool existed = false;
		FileHashes hashes;
		NTSTATUS res;

		// Lookup the file name from the handle
		length = path_from_object_attributes(ObjectAttributes, fname, MAX_PATH_PLUS_TOLERANCE);
		length = ensure_absolute_path(fname, fname, length);

		// Check if the file exists and only notify the agent if it does. Otherwise this attempt will fail and we are not interested in calculating its hash.
		//NTSTATUS res = sNtOpenFile_Hook.fnNtOpenFile(&hfile, GENERIC_READ, ObjectAttributes, &sb, FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE, 0x00000040 /*FILE_NON_DIRECTORY_FILE */);
		hfile = CreateFile(fname,
			GENERIC_READ,
			FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
			NULL,
			OPEN_EXISTING,
			FILE_FLAG_SEQUENTIAL_SCAN,
			NULL);

		if (hfile != INVALID_HANDLE_VALUE) {
			existed = true;
			sNtClose_Hook.fnNtClose(hfile);

			// Calculate both MD5 and SHA1 for the file at this stage.
			calc_file_hashes(fname, &hashes);		
		}
		
		/*
		if (existed) {
			swprintf(buffer, _T("Hooked_NtOpenFile on %s, FILE EXISTED -> SHA1: %s, MD5: %s"), fname, hashes.sha1_hex, hashes.md5_hex);
		}
		else {
			swprintf(buffer, _T("Hooked_NtOpenFile on %s, FILE DID NOT EXIST"), fname);
		}
		Log(buffer);
		*/

		// As last step, invoke the original function and notify the logger only if it succeeded. This will reduce the overhead in case of failed functions
		res = sNtOpenFile_Hook.fnNtOpenFile(FileHandle, DesiredAccess, ObjectAttributes, IoStatusBlock, ShareAccess, OpenOptions);
		if (NT_SUCCESS(res)) {
			// The operation succeeded. Thus the caller had enough permissions to open the file with write/execute permissions.
			// We now want to keeo track of operations regarding this file.
			handles.AddHandle(*FileHandle, fname, (OpenOptions & FILE_DELETE_ON_CLOSE) == FILE_DELETE_ON_CLOSE);

			FileOpenMessage m = FileOpenMessage();
			m.SetPath(fname);
			m.SetExists(existed);
			if (existed) {
				m.SetMD5(hashes.md5_hex);
				m.SetSHA1(hashes.sha1_hex);
				m.SetSize(hashes.size);
			}
			else {
				m.SetMD5(L"");
				m.SetSHA1(L"");
				m.SetSize(0);
			}

			// Let the agent know about this. This will block until the notification is received by the Agent.
			NotifyAgent(m);
		}

		return res;

	}
	else {
		// This operation is not interesting for us. Just invoke the original function
		return sNtOpenFile_Hook.fnNtOpenFile(FileHandle, DesiredAccess, ObjectAttributes, IoStatusBlock, ShareAccess, OpenOptions);
	}
}

#pragma endregion

#pragma region NtCreateFile
static int WINAPI Hooked_NtCreateFile(
	_Out_    PHANDLE            FileHandle,
	_In_     ACCESS_MASK        DesiredAccess,
	_In_     POBJECT_ATTRIBUTES ObjectAttributes,
	_Out_    PIO_STATUS_BLOCK   IoStatusBlock,
	_In_opt_ PLARGE_INTEGER     AllocationSize,
	_In_     ULONG              FileAttributes,
	_In_     ULONG              ShareAccess,
	_In_     ULONG              CreateDisposition,
	_In_     ULONG              CreateOptions,
	_In_opt_ PVOID              EaBuffer,
	_In_     ULONG              EaLength
) {
	// NtCreateFile might be used both for CREATING a new file or for OPENING an existing file.
	// We want to discriminate these cases. If opening a file, we first want to calculate the hash of the file being opened, then we
	// deroute the execution to the original function and notify the caller about that.
	// On the opposite, if the file does not already exist, we'll skip hash calculation and immediately notify the Agent.

	// Only perfom HASH checks and notify the GuestAgent if the file access has write permissions. Also skip special devices and folders operations.
	if (is_directory_objattr(ObjectAttributes) == 0 && is_ignored_file_unicode(ObjectAttributes->ObjectName->Buffer,
		ObjectAttributes->ObjectName->Length / sizeof(wchar_t)) == 0 && (access_mask_write_perm(DesiredAccess) || access_mask_execute_perm(DesiredAccess))) {
		HANDLE hfile;
		IO_STATUS_BLOCK sb;
		uint32_t length;
		wchar_t fname[MAX_PATH_PLUS_TOLERANCE];
		//wchar_t buffer[256 + MAX_PATH_PLUS_TOLERANCE];
		bool existed = false;
		FileHashes hashes;
		NTSTATUS res;

		// Lookup the file name from the handle
		length = path_from_object_attributes(ObjectAttributes, fname, MAX_PATH_PLUS_TOLERANCE);
		length = ensure_absolute_path(fname, fname, length);

		// If the file exists, calculate its hash before invoking the original function. This is an OPEN attempt. To check whether this is the case, use the NtOpenFile.
		hfile = CreateFile(fname,
			GENERIC_READ,
			FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
			NULL,
			OPEN_EXISTING,
			FILE_FLAG_SEQUENTIAL_SCAN,
			NULL);

		if (hfile != INVALID_HANDLE_VALUE) {
			uint32_t length;

			// File does already exist. Calculate its hash.
			existed = true;

			// We don't need the handle any longer. Existance check has been performed.
			CloseHandle(hfile);

			// Calculate both MD5 and SHA1 for the file at this stage.
			calc_file_hashes(fname, &hashes);
		}

		/*
		if (existed) {
			swprintf(buffer, _T("Hooked_NtCreateFile on %s, FILE EXISTED -> hash: %s"), fname, hashes.sha1_hex, hashes.md5_hex);
		}
		else {
			DWORD err = GetLastError();
			swprintf(buffer, _T("Hooked_NtCreateFile on %s, FILE DID NOT EXIST"), fname);
		}
		Log(buffer);
		*/

		// As last step, invoke the original function and notify the logger only if it succeeded. This will reduce the overhead in case of failed functions
		res = sNtCreateFile_Hook.fnNtCreateFile(FileHandle, DesiredAccess, ObjectAttributes, IoStatusBlock, AllocationSize, FileAttributes, ShareAccess, CreateDisposition, CreateOptions, EaBuffer, EaLength);
		if (NT_SUCCESS(res)) {
			// The operation succeeded. Thus the caller had enough permissions to open the file with write/execute permissions.
			// We now want to keeo track of operations regarding this file.
			handles.AddHandle(*FileHandle, fname, (CreateOptions & FILE_DELETE_ON_CLOSE) == FILE_DELETE_ON_CLOSE);

			FileOpenMessage m = FileOpenMessage();
			m.SetPath(fname);
			m.SetExists(existed);
			if (existed) {
				m.SetMD5(hashes.md5_hex);
				m.SetSHA1(hashes.sha1_hex);
				m.SetSize(hashes.size);
			}
			else {
				m.SetMD5(L"");
				m.SetSHA1(L"");
				m.SetSize(0);
			}
			// Let the agent know about this. This will block until the notification is received by the Agent.
			NotifyAgent(m);
		}
		
		return res;
	}
	else {
		// No need to hook. Immediately invoke the orinal function
		return sNtCreateFile_Hook.fnNtCreateFile(FileHandle, DesiredAccess, ObjectAttributes, IoStatusBlock, AllocationSize, FileAttributes, ShareAccess, CreateDisposition, CreateOptions, EaBuffer, EaLength);
	}
	//return sNtCreateFile_Hook.fnNtCreateFile(FileHandle, DesiredAccess, ObjectAttributes, IoStatusBlock, AllocationSize, FileAttributes, ShareAccess, CreateDisposition, CreateOptions, EaBuffer, EaLength);
}
#pragma endregion

#pragma region NtDeleteFile
static int WINAPI Hooked_NtDeleteFile(
	__in POBJECT_ATTRIBUTES ObjectAttributes
) {
	HANDLE hfile;
	IO_STATUS_BLOCK sb;
	uint32_t length;
	wchar_t fname[MAX_PATH_PLUS_TOLERANCE];
	//wchar_t buffer[256 + MAX_PATH_PLUS_TOLERANCE];
	bool existed = false;
	FileHashes hashes;
	NTSTATUS res;

	// Check if the file exists and only notify the agent if it does. Otherwise this attempt will fail and we are not interested in calculating its hash.
	res = sNtOpenFile_Hook.fnNtOpenFile(&hfile, GENERIC_READ, ObjectAttributes, &sb, FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE, 0x00000040 /*FILE_NON_DIRECTORY_FILE */);
	if (res == STATUS_SUCCESS) {
		existed = true;
		sNtClose_Hook.fnNtClose(hfile);

		// Calculate both MD5 and SHA1 for the file at this stage.
		calc_file_hashes(fname, &hashes);
	}

	/*
	if (existed) {
		swprintf(buffer, _T("Hooked_NtDeleteFile on %s, FILE EXISTED -> SHA1: %s, MD5: %s"), fname, hashes.sha1_hex, hashes.md5_hex);
	}
	else {
		swprintf(buffer, _T("Hooked_NtDeleteFile on %s, FILE DID NOT EXIST"), fname);
	}
	Log(buffer);
	*/

	// As last step, invoke the original function and notify the logger only if it succeeded. This will reduce the overhead in case of failed functions
	res = sNtDeleteFile_Hook.fnNtDeleteFile(ObjectAttributes);
	if (NT_SUCCESS(res)) {
		FileDeleteMessage m = FileDeleteMessage();
		m.SetPath(fname);
		m.SetExists(hashes.file_existed);
		if (existed) {
			m.SetMD5(hashes.md5_hex);
			m.SetSHA1(hashes.sha1_hex);
			m.SetSize(hashes.size);
		}
		else {
			m.SetMD5(L"");
			m.SetSHA1(L"");
			m.SetSize(0);
		}
		// Let the agent know about this. This will block until the notification is received by the Agent.
		NotifyAgent(m);
	}

	return res;
}
#pragma endregion

#pragma region NtSetInformationFile
static int WINAPI Hooked_NtSetInformationFile(
	__in  HANDLE                 FileHandle,
	__out PIO_STATUS_BLOCK       IoStatusBlock,
	__in  PVOID                  FileInformation,
	__in  ULONG                  Length,
	__in  FILE_INFORMATION_CLASS FileInformationClass
) {
	// Check if the file has been opened with enough permissions and if we are interested in that.
	// TODO.
	TCHAR old_path[MAX_PATH_PLUS_TOLERANCE];
	TCHAR new_path[MAX_PATH_PLUS_TOLERANCE];
	TCHAR buff[MAX_PATH_PLUS_TOLERANCE];
	uint32_t len = 0;
	NTSTATUS res;
	PFILE_RENAME_INFORMATION info = NULL;
	FileHashes hashes;
	FileRenameMessage m;

	new_path[0] = '\0';

	switch (FileInformationClass) {
	case FILE_INFORMATION_CLASS::FileRenameInformation:
		// This is a rename attempt: retrieve both the old path and the new path.
		if (!handles.GetPathFromHandle(FileHandle, old_path, MAX_PATH_PLUS_TOLERANCE, NULL)) {
			// This was an error. We are not interested in continuing our analysis here.
			return sNtSetInformationFile_Hook.fnNtSetInformationFile(FileHandle, IoStatusBlock, FileInformation, Length, FileInformationClass);
		}

		calc_file_hashes(old_path, &hashes);

		// Now call the API and notify the client only if the operation is successful.
		res = sNtSetInformationFile_Hook.fnNtSetInformationFile(FileHandle, IoStatusBlock, FileInformation, Length, FileInformationClass);
		if (res != STATUS_SUCCESS) return res;

		// We expect a valid FILE_RENAME_INFORMATION structure as FileInformation parameter.
		info = ((PFILE_RENAME_INFORMATION)FileInformation);

		// We want to provide FULL qualified domains for both "Old" and "New" file names/locations.
		// If the RootDirectory isn't null, it contains an HANDLE to a directory which is the parent of the destination folder
		if (info->RootDirectory != NULL) {
			GetFinalPathNameByHandle(info->RootDirectory, new_path, MAX_PATH_PLUS_TOLERANCE, FILE_NAME_NORMALIZED);
			//len = path_from_handle(info->RootDirectory, new_path, MAX_PATH_PLUS_TOLERANCE);
			len = ensure_absolute_path(new_path, new_path, len);
		}

		// For alignment necessity, info->Filename might contain more characters than necessary. We need to truncate such string.
		memcpy(buff, info->FileName, info->FileNameLength);
		// Now add the terminator
		buff[info->FileNameLength / sizeof(wchar_t)] = '\0';

		// TODO: verify what happens in case of simple file rename without changing root directory.
		len = ensure_absolute_path(buff, buff, info->FileNameLength);
		// Combine now the root with the file name. 
		PathCombine(new_path, new_path, buff);

		// Now notify the Agent about the rename event.
		m.SetOPath(old_path);
		m.SetFPath(new_path);
		m.SetMD5(hashes.md5_hex);
		m.SetSHA1(hashes.sha1_hex);
		m.SetSize(hashes.size);

		// Let the agent know about this. This will block until the notification is received by the Agent.
		NotifyAgent(m);

		return res;

	case FILE_INFORMATION_CLASS::FileDispositionInformation:
		// This is a delete attempt
		// Raise the associated flag in the handle map
		res = sNtSetInformationFile_Hook.fnNtSetInformationFile(FileHandle, IoStatusBlock, FileInformation, Length, FileInformationClass);
		if (res == STATUS_SUCCESS)
			handles.SetHandleMarkedForDeletion(FileHandle);
		return res;
	default:
		// Notinteresting
		return sNtSetInformationFile_Hook.fnNtSetInformationFile(FileHandle, IoStatusBlock, FileInformation, Length, FileInformationClass);
	}

}
#pragma endregion

// Registry functions
#pragma region NtCreateKey
static int WINAPI Hooked_NtCreateKey(
	__out      PHANDLE            KeyHandle,
	__in       ACCESS_MASK        DesiredAccess,
	__in       POBJECT_ATTRIBUTES ObjectAttributes,
	__reserved ULONG              TitleIndex,
	__in_opt   PUNICODE_STRING    Class,
	__in       ULONG              CreateOptions,
	__out_opt  PULONG             Disposition
	) {
	
	TCHAR path[MAX_PATH];

	// From https://msdn.microsoft.com/en-us/library/windows/hardware/ff566425(v=vs.85).aspx
	// The ZwCreateKey routine creates a new registry key or opens an existing one.
	// We only want to get notified every time a new key is created. Such information is easily retrieved after the call to NTCreateKey, which writes in "disposition" the 
	// outcome of the operation
	NTSTATUS res;
	ULONG myDisposition;

	// If no Disposition value has been provided, overwrite the parameter with the address of a local variable, so we'll know what the outcome is.
	if (Disposition == NULL)
		Disposition = &myDisposition;

	// Call the original function
	res = sNtCreateKey_Hook.fnNtCreateKey(KeyHandle, DesiredAccess, ObjectAttributes, TitleIndex, Class, CreateOptions, Disposition);

	// If the operation succeeded and the outcome is a "new key has been created", notify the Agent
	if(res == STATUS_SUCCESS && *Disposition == REG_CREATED_NEW_KEY) {
		// Retrieve all the info we need and send the notification to the Agent.
		registry_path_from_object_attributes(ObjectAttributes, path, MAX_PATH);
		
		// Now notity the Agent
		RegistryNewKeyMessage m = RegistryNewKeyMessage();
		m.SetPath(path);
		NotifyAgent(m);
	}
	
	// In any case, return the appropriate status
	return res;

}
#pragma endregion

#pragma region NtDeleteKey
static int WINAPI Hooked_NtDeleteKey(
	__in HANDLE KeyHandle
	) {
	
	NTSTATUS res;
	wchar_t path[MAX_PATH];

	// We do not store info about Registry handles. In case of deletion we need to retrieve them BEFORE attempting the deletion.
	// This is an overhead that might be fixed by introducing an HANDLE MAP.
	path[0] = '\0';
	if (registry_path_from_handle(KeyHandle, path, MAX_PATH) == 0) {
		// We got an error.
		Log(L"Cannot retrieve registry path for an handle specified in NtDeleteKey", true);
	}
	
	// We want to notify the Agent only if the operation succedeed, so we invoke the native function and log only success cases.
	res = sNtDeleteKey_Hook.fnNtDeleteKey(KeyHandle);
	
	if (res == STATUS_SUCCESS) {
		// Operation was successful, so retrieve the key path and notify the Agent.
		RegistryDeleteKeyMessage m = RegistryDeleteKeyMessage();
		m.SetPath(path);
		NotifyAgent(m);		
	}
	
	return res;
	
}
#pragma endregion

#pragma region NtSetValueKey
static int WINAPI Hooked_NtsetValueKey(
	__in     HANDLE          KeyHandle,
	__in     PUNICODE_STRING ValueName,
	__in_opt ULONG           TitleIndex,
	__in     ULONG           Type,
	__in_opt PVOID           Data,
	__in     ULONG           DataSize
	) {
	// https://msdn.microsoft.com/en-us/library/windows/hardware/ff567109(v=vs.85).aspx
	// The ZwSetValueKey routine creates or replaces a registry key's value entry.

	DWORD mDataSize;
	wchar_t key_path[MAX_PATH];
	BYTE *buffer;
	NTSTATUS res;
	HANDLE mKeyHandle = NULL;
	wchar_t* cstr_valuename = NULL;
	PKEY_VALUE_FULL_INFORMATION old_val;

	// There are three possible outcomes of this function:
	// 1. Failure
	// 2. Success, value is created
	// 3. Success, value is overwritten
	// We want to notify the agent regarding cases 2 and 3. In particular, when case 3 happens, we also want to retrieve the previous value associated to the name.
	key_path[0] = '\0';
	if (registry_path_from_handle(KeyHandle, key_path, MAX_PATH) == 0) {
		Log(L"Cannot retrieve registry path while hooking NtSetValueKey", true);
		return sNtsetValueKey_Hook.fnNtsetValueKey(KeyHandle, ValueName, TitleIndex, Type, Data, DataSize);
	}

	// We now need to check if the ValueName already exists. If so, we should backup its contents.
	// In most of the cases we expect that the handle has also read permissions alongside write permissions. If that is not the case,
	// we need to open the key again with read permission using a different handle. 
	
	// Check if we have read permissions on the valuename and how much data do we need to allocate
	res = NtQueryValueKey(KeyHandle, ValueName, KEY_VALUE_INFORMATION_CLASS::KeyValueFullInformation, NULL, 0, &mDataSize);
	// If we take the first branch, it means we DO HAVE read permissions and we also learn this is a new value-name creation attempt.
	// Let's handle the event directly here.
	if (res == 0xC0000034 /* STATUS_OBJECT_NAME_NOT_FOUND */) {
		// CASE 1: This is an attempt to create a new ValueName! Only notify the Agent upon successful creation.
		res = sNtsetValueKey_Hook.fnNtsetValueKey(KeyHandle, ValueName, TitleIndex, Type, Data, DataSize);
		if (res == STATUS_SUCCESS) {
			// Notify a new value name was created.
			RegistryNewValueMessage m = RegistryNewValueMessage();
			m.SetKeyPath(key_path);

			// Unfortunately, UNICODE_STRING is handled as a structure containing LENGTH:DATA, and data is not guaranteed to be null-terminated. Let's convert it here.
			// ValueName->Length indicates the number of bytes pointed by the buffer NOT INCLUDING ANY NULL-TERMINATOR
			cstr_valuename = new wchar_t[ValueName->Length / sizeof(wchar_t) + 1]; // Length in wchar_t + termination char
			memcpy(cstr_valuename, ValueName->Buffer, ValueName->Length); // Copy the string
			cstr_valuename[ValueName->Length / sizeof(wchar_t)] = '\0'; // Ensure is null-terminated
			m.SetValueName(cstr_valuename);
			m.SetValueData(Type, Data, DataSize);

			// Notify the agent!
			NotifyAgent(m);

			// Delete the buffer only when the message has been notified to the Agent.
			delete[] cstr_valuename;

			// Case 1 Handled.
			return res;
		}
	}
	
	// Otherwise, if the error is different from OVERFLOW and BUFFER_TOO_SMALL, it means we don't have enough permissions to access the key. 
	// So let's open another handle to the object with READ PERMISSIONS
	else if (res != 0x80000005 /* STATUS_BUFFER_OVERFLOW */ && res != 0xC0000023 /* STATUS_BUFFER_TOO_SMALL */) {
		// Attempt 2: create a new handle and retry
		OBJECT_ATTRIBUTES KeyAttributes;
		UNICODE_STRING str;

		// RtlUnicodeStringInitEx does the following: https://msdn.microsoft.com/en-us/library/windows/hardware/ff562958(v=vs.85).aspx
		str.Length = lstrlenW(key_path);
		str.MaximumLength = lstrlenW(key_path) + 2;
		str.Buffer = key_path;

		InitializeObjectAttributes(&KeyAttributes, &str, 0x00000040 /* OBJ_CASE_INSENSITIVE */, NULL, NULL);
		// We probably didn't have permissions to do so. Open a new handle and perform the action again.
		res = NtOpenKey(&mKeyHandle, KEY_READ, &KeyAttributes);
		if (res != STATUS_SUCCESS) {
			// We failed
			Log(L"Cannot open a second handle to the key for retrieving previoud value in NtSetValueKey", true);
			return sNtsetValueKey_Hook.fnNtsetValueKey(KeyHandle, ValueName, TitleIndex, Type, Data, DataSize);
		}

		// We now have a new key handle: retry!
		res = NtQueryValueKey(mKeyHandle, ValueName, KEY_VALUE_INFORMATION_CLASS::KeyValueFullInformation, NULL, 0, &mDataSize);
		if (res != 0x80000005 /* STATUS_BUFFER_OVERFLOW */ && res != 0xC0000023 /* STATUS_BUFFER_TOO_SMALL */) {
			sNtClose_Hook.fnNtClose(mKeyHandle);
			Log(L"Cannot retrieve data len from second handle opened in NtSetValueKey", true);
			return sNtsetValueKey_Hook.fnNtsetValueKey(KeyHandle, ValueName, TitleIndex, Type, Data, DataSize);
		}
	}
	
	// Allocate enough data to store all the data NtQueryValueKey returns
	buffer = new BYTE[mDataSize];

	// Call it again with the new buffer
	if (NtQueryValueKey(mKeyHandle == NULL ? KeyHandle : mKeyHandle, ValueName, KEY_VALUE_INFORMATION_CLASS::KeyValueFullInformation, buffer, mDataSize, &mDataSize) != STATUS_SUCCESS) {
		// we failed.
		delete[] buffer;

		if (mKeyHandle != NULL)
			sNtClose_Hook.fnNtClose(mKeyHandle);

		Log(L"Cannot retrieve old valuename value during NtSetValueKey", true);
		return sNtsetValueKey_Hook.fnNtsetValueKey(KeyHandle, ValueName, TitleIndex, Type, Data, DataSize);
	}
	
	old_val = (PKEY_VALUE_FULL_INFORMATION)buffer;
	// At this stage, we assume this is a OVERWRITE ATTEMPT. Therefore we need to store the previous value and the new ones.
	// Then we call the original function and, in case of success, we notify the Agent.
	RegistryModifiedValueMessage m = RegistryModifiedValueMessage();
	m.SetKeyPath(key_path);

	// Set old value name
	cstr_valuename = new wchar_t[old_val->NameLength / sizeof(wchar_t) + 1]; // Length in wchar_t + termination char
	memcpy(cstr_valuename, old_val->Name, old_val->NameLength); // Copy the string
	cstr_valuename[old_val->NameLength / sizeof(wchar_t)] = '\0'; // Ensure is null-terminated
	m.SetOldValueName(cstr_valuename);
	delete[] cstr_valuename;

	// Set old value data
	m.SetOldValueData(old_val->Type, buffer + old_val->DataOffset, old_val->DataLength);

	// Now invoke the real function and if it succeeds, notify the Agent
	res = sNtsetValueKey_Hook.fnNtsetValueKey(KeyHandle, ValueName, TitleIndex, Type, Data, DataSize);
	if (res == STATUS_SUCCESS) {
		// Also log the new data
		// Set new value name
		cstr_valuename = new wchar_t[ValueName->Length / sizeof(wchar_t) + 1]; // Length in wchar_t + termination char
		memcpy(cstr_valuename, ValueName->Buffer, ValueName->Length); // Copy the string
		cstr_valuename[ValueName->Length / sizeof(wchar_t)] = '\0'; // Ensure is null-terminated
		m.SetNewValueName(cstr_valuename);
		delete[] cstr_valuename;

		// Set new value data
		m.SetNewValueData(Type, Data, DataSize);

		// Finally notify the agent
		NotifyAgent(m);
	}

	delete[] buffer;
	if (mKeyHandle != NULL)
		sNtClose_Hook.fnNtClose(mKeyHandle);
	
	return res;
}
#pragma endregion

#pragma region NtDeleteValueKey
static int WINAPI Hooked_NtDeleteValueKey(
	__in HANDLE          KeyHandle,
	__in PUNICODE_STRING ValueName
	) {

	DWORD mDataSize;
	wchar_t key_path[MAX_PATH];
	BYTE *buffer;
	NTSTATUS res;
	HANDLE mKeyHandle = NULL;
	wchar_t* cstr_valuename = NULL;
	PKEY_VALUE_FULL_INFORMATION old_val;

	// First retrieve some info about the valuename being deleted such as Key and value name
	key_path[0] = '\0';
	if (registry_path_from_handle(KeyHandle, key_path, MAX_PATH) == 0) {
		Log(L"Cannot retrieve registry path while hooking NtDeleteValueKey", true);
		return sNtDeleteValueKey_Hook.fnNtDeleteValueKey(KeyHandle, ValueName);
	}

	// Check if we have read permissions on the valuename and how much data do we need to allocate
	res = NtQueryValueKey(KeyHandle, ValueName, KEY_VALUE_INFORMATION_CLASS::KeyValueFullInformation, NULL, 0, &mDataSize);
	// If the error is different from OVERFLOW and BUFFER_TOO_SMALL, it means we don't have enough permissions to access the key. 
	// So let's open another handle to the object with READ PERMISSIONS
	if (res != 0x80000005 /* STATUS_BUFFER_OVERFLOW */ && res != 0xC0000023 /* STATUS_BUFFER_TOO_SMALL */) {
		// Attempt 2: create a new handle and retry
		OBJECT_ATTRIBUTES KeyAttributes;
		UNICODE_STRING str;

		// RtlUnicodeStringInitEx does the following: https://msdn.microsoft.com/en-us/library/windows/hardware/ff562958(v=vs.85).aspx
		str.Length = lstrlenW(key_path);
		str.MaximumLength = lstrlenW(key_path) + 2;
		str.Buffer = key_path;

		InitializeObjectAttributes(&KeyAttributes, &str, 0x00000040 /* OBJ_CASE_INSENSITIVE */, NULL, NULL);
		// We probably didn't have permissions to do so. Open a new handle and perform the action again.
		res = NtOpenKey(&mKeyHandle, KEY_READ, &KeyAttributes);
		if (res != STATUS_SUCCESS) {
			// We failed
			Log(L"Cannot open a second handle to the key for retrieving previoud value in NtSetValueKey", true);
			return sNtDeleteValueKey_Hook.fnNtDeleteValueKey(KeyHandle, ValueName);
		}

		// We now have a new key handle: retry!
		res = NtQueryValueKey(mKeyHandle, ValueName, KEY_VALUE_INFORMATION_CLASS::KeyValueFullInformation, NULL, 0, &mDataSize);
		if (res != 0x80000005 /* STATUS_BUFFER_OVERFLOW */ && res != 0xC0000023 /* STATUS_BUFFER_TOO_SMALL */) {
			sNtClose_Hook.fnNtClose(mKeyHandle);
			Log(L"Cannot retrieve data len from second handle opened in NtDeleteValueKey", true);
			return sNtDeleteValueKey_Hook.fnNtDeleteValueKey(KeyHandle, ValueName);
		}
	}

	// Allocate enough data to store all the data NtDeleteValueKey returns
	buffer = new BYTE[mDataSize];

	// Call it again with the new buffer
	if (NtQueryValueKey(mKeyHandle == NULL ? KeyHandle : mKeyHandle, ValueName, KEY_VALUE_INFORMATION_CLASS::KeyValueFullInformation, buffer, mDataSize, &mDataSize) != STATUS_SUCCESS) {
		// we failed.
		delete[] buffer;

		if (mKeyHandle != NULL)
			sNtClose_Hook.fnNtClose(mKeyHandle);

		Log(L"Cannot retrieve old valuename value during NtDeleteValueKey", true);
		return sNtDeleteValueKey_Hook.fnNtDeleteValueKey(KeyHandle, ValueName);
	}

	old_val = (PKEY_VALUE_FULL_INFORMATION)buffer;

	// Now invoke the real function and if succeeds, notify the agent
	res = sNtDeleteValueKey_Hook.fnNtDeleteValueKey(KeyHandle, ValueName);
	if (res == STATUS_SUCCESS) {
		RegistryDeletedValueMessage m = RegistryDeletedValueMessage();
		m.SetOldKeyPath(key_path);
		cstr_valuename = new wchar_t[old_val->NameLength / sizeof(wchar_t) + 1]; // Length in wchar_t + termination char
		memcpy(cstr_valuename, old_val->Name, old_val->NameLength); // Copy the string
		cstr_valuename[old_val->NameLength / sizeof(wchar_t)] = '\0'; // Ensure is null-terminated
		m.SetOldValueName(cstr_valuename);
		m.SetOldValueData(old_val->Type, buffer + old_val->DataOffset, old_val->DataLength);
		
		NotifyAgent(m);
	}

	delete[] buffer;
	return res;
}
#pragma endregion

#pragma region NtRenameKey
static int WINAPI Hooked_NtRenameKey(
	_In_ HANDLE          KeyHandle,
	_In_ PUNICODE_STRING NewName
	) {
	
	// Changes the name of the specified registry key.
	// https://msdn.microsoft.com/en-us/library/cc512138(v=vs.85).aspx
	// We just want to save the previous KeyName and the new value.
	NTSTATUS res;
	wchar_t key_path[MAX_PATH];
	wchar_t* new_key_name;

	// Retrieve the original name
	key_path[0] = '\0';
	if (registry_path_from_handle(KeyHandle, key_path, MAX_PATH) == 0) {
		Log(L"Cannot retrieve registry path while hooking NtRenameKey", true);
		return sNtRenameKey_Hook.fnNtRenameKey(KeyHandle, NewName);
	}

	// Now invoke the function
	res = sNtRenameKey_Hook.fnNtRenameKey(KeyHandle, NewName);

	// If it was successful, build the corresponding notification message and notify the Agent
	if (res == STATUS_SUCCESS) {
		unsigned int pos;
		// Unfortunately, the NewName contains only the name for the Key to be renamed, not the entire path. 
		// Therefore, we need to rebuild the entire path before saving that.
		std::wstring tmp_path = std::wstring(key_path);
		pos = tmp_path.find_last_of('\\');
		if (pos == std::wstring::npos) {
			// we failed
			Log(L"Cannot retrieve parent path in NtRenameKey", true);
			return res;
		}
		
		// Truncate the string
		tmp_path.resize(pos);

		new_key_name = new wchar_t[NewName->Length / sizeof(wchar_t) + 1]; // Length in wchar_t + termination char
		memcpy(new_key_name, NewName->Buffer, NewName->Length); // Copy the string
		new_key_name[NewName->Length/ sizeof(wchar_t)] = '\0'; // Ensure is null-terminated

		// Now add the newname
		tmp_path.append(L"\\");
		tmp_path.append(new_key_name);

		RegistryRenamedKeyMessage m = RegistryRenamedKeyMessage();
		m.SetOldPath(key_path);
		m.SetNewPath((wchar_t*)tmp_path.c_str());

		NotifyAgent(m);
	}

	return res;
}
#pragma endregion