#pragma once
#include <stdio.h>
#include <windows.h>
#include <tchar.h>
#include <string.h>
#include <psapi.h>
#include <strsafe.h>
#include <Shlwapi.h>
#include <stdint.h>
#include "nt_structs.h"
#include <Wincrypt.h>

#define FLAG_NONE           0
#define FLAG_BEGINS_WITH    1
#define S(s, f) {L##s, sizeof(s)-1, f}


#pragma comment( lib  , "Shlwapi.lib"  )

// Blacklist of files to be monitored as done by Cuckoo mon.
static struct _ignored_file_t {
	const wchar_t   *unicode;
	unsigned int    length;
	unsigned int    flags;
} g_ignored_files[] = {
	S("\\??\\PIPE\\lsarpc", FLAG_NONE),
	S("\\??\\IDE#", FLAG_BEGINS_WITH),
	S("\\??\\STORAGE#", FLAG_BEGINS_WITH),
	S("\\??\\MountPointManager", FLAG_NONE),
	S("\\??\\root#", FLAG_BEGINS_WITH),
	S("\\Device\\", FLAG_BEGINS_WITH),
};

#define InitializeObjectAttributes(p, n, a, r, s) \
{ \
	(p)->Length = sizeof(OBJECT_ATTRIBUTES); \
	(p)->RootDirectory = r; \
	(p)->Attributes = a; \
	(p)->ObjectName = n; \
	(p)->SecurityDescriptor = s; \
	(p)->SecurityQualityOfService = NULL; \
}

typedef struct _KEY_VALUE_FULL_INFORMATION {
	ULONG TitleIndex;
	ULONG Type;
	ULONG DataOffset;
	ULONG DataLength;
	ULONG NameLength;
	WCHAR Name[1];
} KEY_VALUE_FULL_INFORMATION, *PKEY_VALUE_FULL_INFORMATION;

typedef struct FILE_HASHES {
	TCHAR sha1_hex[41];
	TCHAR md5_hex[33];
	BYTE sha1_digest[20];
	BYTE md5_digest[16];
	bool file_existed = false;
	unsigned long size = 0;
} FileHashes;

void Log(const TCHAR* error, bool is_error = false);
BOOL is_directory_objattr(const OBJECT_ATTRIBUTES *obj);
int is_ignored_file_unicode(const wchar_t *fname, int length);
// Use GetFinalPathNameByHandle() instead
//uint32_t path_from_handle(HANDLE handle, wchar_t *path, uint32_t path_buffer_len);
uint32_t path_from_object_attributes(const OBJECT_ATTRIBUTES *obj, wchar_t *path, uint32_t buffer_length);
int ensure_absolute_path(wchar_t *out, const wchar_t *in, int length);
bool access_mask_write_perm(ACCESS_MASK mask);
bool access_mask_execute_perm(ACCESS_MASK mask);
bool calc_file_hashes(__in LPCTSTR filename, __out FileHashes* hash_res);


typedef NTSTATUS (WINAPI *lpNtQueryKey)(
	_In_      HANDLE                KeyHandle,
	_In_      DWORD KeyInformationClass,
	_Out_opt_ PVOID                 KeyInformation,
	_In_      ULONG                 Length,
	_Out_     PULONG                ResultLength
);
static lpNtQueryKey NtQueryKey=NULL;

#pragma region NtQueryValueKey
typedef enum _KEY_VALUE_INFORMATION_CLASS {
	KeyValueBasicInformation = 0,
	KeyValueFullInformation,
	KeyValuePartialInformation,
	KeyValueFullInformationAlign64,
	KeyValuePartialInformationAlign64,
	MaxKeyValueInfoClass
} KEY_VALUE_INFORMATION_CLASS;
typedef NTSTATUS(WINAPI *lpNtQueryValueKey)(
	_In_      HANDLE                      KeyHandle,
	_In_      PUNICODE_STRING             ValueName,
	_In_      KEY_VALUE_INFORMATION_CLASS KeyValueInformationClass,
	_Out_opt_ PVOID                       KeyValueInformation,
	_In_      ULONG                       Length,
	_Out_     PULONG                      ResultLength
	);
static lpNtQueryValueKey _NtQueryValueKey = NULL;
NTSTATUS NtQueryValueKey(
	_In_      HANDLE                      KeyHandle,
	_In_      PUNICODE_STRING             ValueName,
	_In_      KEY_VALUE_INFORMATION_CLASS KeyValueInformationClass,
	_Out_opt_ PVOID                       KeyValueInformation,
	_In_      ULONG                       Length,
	_Out_     PULONG                      ResultLength
	);
#pragma endregion

#pragma region NtOpenKey
typedef NTSTATUS(WINAPI *lpNtOpenKey)(
	_Out_ PHANDLE            KeyHandle,
	_In_  ACCESS_MASK        DesiredAccess,
	_In_  POBJECT_ATTRIBUTES ObjectAttributes
	);
static lpNtOpenKey _NtOpenKey = NULL;
NTSTATUS NtOpenKey(
	_Out_ PHANDLE            KeyHandle,
	_In_  ACCESS_MASK        DesiredAccess,
	_In_  POBJECT_ATTRIBUTES ObjectAttributes
	);
#pragma endregion

uint32_t registry_path_from_object_attributes(const OBJECT_ATTRIBUTES *obj,TCHAR *path, uint32_t buffer_length);
uint32_t registry_path_from_handle(HANDLE KeyHandle, wchar_t *path, uint32_t tchars_in_buff);