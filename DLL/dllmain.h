#pragma once
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "NktHookLib.h"
#define DISALLOW_REENTRANCY
#include "nt_structs.h"
#include "LogMessage.h"

//-----------------------------------------------------------
#if defined _M_IX86
#ifdef _DEBUG
#pragma comment (lib, "NktHookLib_Debug.lib")
#else //_DEBUG
#pragma comment (lib, NktHookLib.lib)
#endif //_DEBUG
#elif defined _M_X64
#ifdef _DEBUG
#pragma comment (lib, "NktHookLib64_Debug.lib")
#else //_DEBUG
#pragma comment (lib, "NktHookLib64.lib")
#endif //_DEBUG
#else
#error Unsupported platform
#endif

void NotifyAgent(LogMessage& m);

// ------------  Definition of hooked functions ------------

// File operations
#pragma region NtClose
typedef NTSTATUS(WINAPI *lpfnNtClose)(
	__in HANDLE FileHandle
	);
static int WINAPI Hooked_NtClose(
	__in HANDLE FileHandle
);

static struct {
	SIZE_T nHookId;
	lpfnNtClose fnNtClose;
} sNtClose_Hook = { 0, NULL };
#pragma endregion

#pragma region NtOpenFile
typedef NTSTATUS (WINAPI *lpfnNtOpenFile)(
	__out PHANDLE            FileHandle,
	__in  ACCESS_MASK        DesiredAccess,
	__in  POBJECT_ATTRIBUTES ObjectAttributes,
	__out PIO_STATUS_BLOCK   IoStatusBlock,
	__in  ULONG              ShareAccess,
	__in  ULONG              OpenOptions
	);
static int WINAPI Hooked_NtOpenFile(
	__out PHANDLE            FileHandle,
	__in  ACCESS_MASK        DesiredAccess,
	__in  POBJECT_ATTRIBUTES ObjectAttributes,
	__out PIO_STATUS_BLOCK   IoStatusBlock,
	__in  ULONG              ShareAccess,
	__in  ULONG              OpenOptions
	);

static struct {
	SIZE_T nHookId;
	lpfnNtOpenFile fnNtOpenFile;
} sNtOpenFile_Hook = { 0, NULL };
#pragma endregion

#pragma region NtCreateFile
typedef NTSTATUS(WINAPI *lpfnNtCreateFile)(
	_Out_    PHANDLE            FileHandle,
	_In_     ACCESS_MASK        DesiredAccess,
	_In_     POBJECT_ATTRIBUTES ObjectAttributes,
	_Out_    PIO_STATUS_BLOCK   IoStatusBlock,
	_In_opt_ PLARGE_INTEGER     AllocationSize,
	_In_     ULONG              FileAttributes,
	_In_     ULONG              ShareAccess,
	_In_     ULONG              CreateDisposition,
	_In_     ULONG              CreateOptions,
	_In_opt_ PVOID              EaBuffer,
	_In_     ULONG              EaLength
	);
static int WINAPI Hooked_NtCreateFile(
	_Out_    PHANDLE            FileHandle,
	_In_     ACCESS_MASK        DesiredAccess,
	_In_     POBJECT_ATTRIBUTES ObjectAttributes,
	_Out_    PIO_STATUS_BLOCK   IoStatusBlock,
	_In_opt_ PLARGE_INTEGER     AllocationSize,
	_In_     ULONG              FileAttributes,
	_In_     ULONG              ShareAccess,
	_In_     ULONG              CreateDisposition,
	_In_     ULONG              CreateOptions,
	_In_opt_ PVOID              EaBuffer,
	_In_     ULONG              EaLength
);

static struct {
	SIZE_T nHookId;
	lpfnNtCreateFile fnNtCreateFile;
} sNtCreateFile_Hook = { 0, NULL };

#pragma endregion

#pragma region NtDeleteFile
typedef NTSTATUS(WINAPI *lpfnNtDeleteFile)(
	__in POBJECT_ATTRIBUTES ObjectAttributes
	);
static int WINAPI Hooked_NtDeleteFile(
	__in POBJECT_ATTRIBUTES ObjectAttributes
);

static struct {
	SIZE_T nHookId;
	lpfnNtDeleteFile fnNtDeleteFile;
} sNtDeleteFile_Hook = { 0, NULL };
#pragma	endregion

#pragma region NtSetInformationFile
typedef NTSTATUS(WINAPI *lpfnNtSetInformationFile)(
	__in  HANDLE                 FileHandle,
	__out PIO_STATUS_BLOCK       IoStatusBlock,
	__in  PVOID                  FileInformation,
	__in  ULONG                  Length,
	__in  FILE_INFORMATION_CLASS FileInformationClass);

static int WINAPI Hooked_NtSetInformationFile(
	__in  HANDLE                 FileHandle,
	__out PIO_STATUS_BLOCK       IoStatusBlock,
	__in  PVOID                  FileInformation,
	__in  ULONG                  Length,
	__in  FILE_INFORMATION_CLASS FileInformationClass
);

static struct {
	SIZE_T nHookId;
	lpfnNtSetInformationFile fnNtSetInformationFile;
} sNtSetInformationFile_Hook = { 0, NULL };
#pragma endregion

// Registry operations
#pragma region NtCreateKey
typedef NTSTATUS(WINAPI *lpfnNtCreateKey)(
	__out      PHANDLE            KeyHandle,
	__in       ACCESS_MASK        DesiredAccess,
	__in       POBJECT_ATTRIBUTES ObjectAttributes,
	__reserved ULONG              TitleIndex,
	__in_opt   PUNICODE_STRING    Class,
	__in       ULONG              CreateOptions,
	__out_opt  PULONG             Disposition
	);

static int WINAPI Hooked_NtCreateKey(
	__out      PHANDLE            KeyHandle,
	__in       ACCESS_MASK        DesiredAccess,
	__in       POBJECT_ATTRIBUTES ObjectAttributes,
	__reserved ULONG              TitleIndex,
	__in_opt   PUNICODE_STRING    Class,
	__in       ULONG              CreateOptions,
	__out_opt  PULONG             Disposition
	);

static struct {
	SIZE_T nHookId;
	lpfnNtCreateKey fnNtCreateKey;
} sNtCreateKey_Hook = { 0, NULL };
#pragma endregion

#pragma region NtDeleteKey
typedef NTSTATUS(WINAPI *lpfnNtDeleteKey)(
	__in HANDLE KeyHandle
	);

static int WINAPI Hooked_NtDeleteKey(
	__in HANDLE KeyHandle
	);

static struct {
	SIZE_T nHookId;
	lpfnNtDeleteKey fnNtDeleteKey;
} sNtDeleteKey_Hook = { 0, NULL };
#pragma endregion

#pragma region NtsetValueKey
typedef NTSTATUS(WINAPI *lpfnNtsetValueKey)(
	__in     HANDLE          KeyHandle,
	__in     PUNICODE_STRING ValueName,
	__in_opt ULONG           TitleIndex,
	__in     ULONG           Type,
	__in_opt PVOID           Data,
	__in     ULONG           DataSize
	);

static int WINAPI Hooked_NtsetValueKey(
	__in     HANDLE          KeyHandle,
	__in     PUNICODE_STRING ValueName,
	__in_opt ULONG           TitleIndex,
	__in     ULONG           Type,
	__in_opt PVOID           Data,
	__in     ULONG           DataSize
	);

static struct {
	SIZE_T nHookId;
	lpfnNtsetValueKey fnNtsetValueKey;
} sNtsetValueKey_Hook = { 0, NULL };
#pragma endregion

#pragma region NtDeleteValueKey
typedef NTSTATUS(WINAPI *lpfnNtDeleteValueKey)(
	__in HANDLE          KeyHandle,
	__in PUNICODE_STRING ValueName
	);

static int WINAPI Hooked_NtDeleteValueKey(
	__in HANDLE          KeyHandle,
	__in PUNICODE_STRING ValueName
	);

static struct {
	SIZE_T nHookId;
	lpfnNtDeleteValueKey fnNtDeleteValueKey;
} sNtDeleteValueKey_Hook = { 0, NULL };
#pragma endregion

#pragma region NtRenameKey
typedef NTSTATUS(WINAPI *lpfnNtRenameKey)(
	_In_ HANDLE          KeyHandle,
	_In_ PUNICODE_STRING NewName
	);

static int WINAPI Hooked_NtRenameKey(
	_In_ HANDLE          KeyHandle,
	_In_ PUNICODE_STRING NewName
	);

static struct {
	SIZE_T nHookId;
	lpfnNtRenameKey fnNtRenameKey;
} sNtRenameKey_Hook = { 0, NULL };
#pragma endregion


// TODO: transaction handling: check if CreateKeyTransacted internally uses CREATEKEY/DELETEKEY and check if we really need to hook it

// Non-hooked Nt utilities
#pragma region FlushBuffersFile
typedef NTSTATUS(WINAPI *lpfnNtFlushBuffersFile)(
	__in  HANDLE           FileHandle,
	__out PIO_STATUS_BLOCK IoStatusBlock
);

static lpfnNtFlushBuffersFile NtFlushBuffersFile;
#pragma endregion

#pragma region NtQueryInformationFile
typedef NTSTATUS(WINAPI *lpfnNtQueryInformationFile)(
	__in  HANDLE                 FileHandle,
	__out PIO_STATUS_BLOCK       IoStatusBlock,
	__out PVOID                  FileInformation,
	__in  ULONG                  Length,
	__in  FILE_INFORMATION_CLASS FileInformationClass
	);
static lpfnNtQueryInformationFile NtQueryInformationFile;
#pragma endregion
