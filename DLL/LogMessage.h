#pragma once
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include <iostream>
#include <Windows.h>

using namespace rapidjson;
using namespace std;

#define LOG_MESSAGE_KEY_ID L"event_id"

// General switches
#define LOG_MESSAGE_KEY_ID_PID L"pid"
#define LOG_MESSAGE_KEY_ID_TID L"tid"

// File events IDS
#define LOG_MESSAGE_KEY_ID_FILEOPENED L"file_opened"
#define LOG_MESSAGE_KEY_ID_FILERENAMED L"file_renamed"
#define LOG_MESSAGE_KEY_ID_FILECLOSED L"file_closed"
#define LOG_MESSAGE_KEY_ID_FILEDELETED L"file_deleted"

// File events PROPS
#define LOG_MESSAGE_KEY_ID_FILE_EXISTED L"existed"
#define LOG_MESSAGE_KEY_ID_FILE_ORIGINAL_PATH L"original_path"
#define LOG_MESSAGE_KEY_ID_FILE_SIZE L"size"
#define LOG_MESSAGE_KEY_ID_FILE_SHA1 L"sha1"
#define LOG_MESSAGE_KEY_ID_FILE_MD5 L"md5"
#define LOG_MESSAGE_KEY_ID_FILE_FINAL_PATH L"final_path"

// Registry events IDS
#define LOG_MESSAGE_KEY_ID_REGISTRY_KEY_CREATED L"key_created"
#define LOG_MESSAGE_KEY_ID_REGISTRY_KEY_DELETED L"key_deleted"
#define LOG_MESSAGE_KEY_ID_REGISTRY_KEY_RENAMED L"key_renamed"
#define LOG_MESSAGE_KEY_ID_REGISTRY_VALUE_CREATED L"value_key_created"
#define LOG_MESSAGE_KEY_ID_REGISTRY_VALUE_EDITED L"value_key_edited"
#define LOG_MESSAGE_KEY_ID_REGISTRY_VALUE_DELETED L"value_key_deleted"

// Registry events PROPS
#define LOG_MESSAGE_KEY_ID_REGISTRY_KEY_PATH L"key_path"
#define LOG_MESSAGE_KEY_ID_REGISTRY_VALUE_NAME L"value_name"
#define LOG_MESSAGE_KEY_ID_REGISTRY_VALUE_TYPE L"value_type"
#define LOG_MESSAGE_KEY_ID_REGISTRY_VALUE_DATA L"value_data"
#define LOG_MESSAGE_KEY_ID_REGISTRY_OLD L"old"
#define LOG_MESSAGE_KEY_ID_REGISTRY_NEW L"new"

#define REGISTRY_VALUE_TYPE_LINK L"LINK"
#define REGISTRY_VALUE_TYPE_RESOURCE_LIST L"RESOURCE_LIST"
#define REGISTRY_VALUE_TYPE_REQUIREMENTS_LIST L"REQUIREMENTS_LIST"
#define REGISTRY_VALUE_TYPE_RESOURCE_DESCRIPTOR L"RESOURCE_DESCRIPTOR"
#define REGISTRY_VALUE_TYPE_DWORD_BIG_ENDIAN L"DWORD_BIG_ENDIAN"
#define REGISTRY_VALUE_TYPE_DWORD L"DWORD"
#define REGISTRY_VALUE_TYPE_QWORD L"QWORD"
#define REGISTRY_VALUE_TYPE_SZ L"SZ"
#define REGISTRY_VALUE_TYPE_EXPAND_SZ L"EXPAND_SZ"
#define REGISTRY_VALUE_TYPE_MULTI_SZ L"MULTI_SZ"
#define REGISTRY_VALUE_TYPE_BINARY L"BINARY"
#define REGISTRY_VALUE_TYPE_NONE L"NONE"



class LogMessage {
private:
	// Identifies the message type
	wstring id;
	uint32_t pid=0;
	uint32_t tid=0;
protected:
	// Method that subclasses need to implement in order to put special
	virtual void WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer) = 0;
public:
	LogMessage(wchar_t* messageType);
	void SetPID(uint32_t pid);
	void SetTID(uint32_t tid);
	wstring ToJson();
};

class FileOpenMessage :public LogMessage {
private:
	bool exists;
	wchar_t path[MAX_PATH];
	uint64_t size;
	wchar_t sha1[41];
	wchar_t md5[33];
public:
	void SetPath(wchar_t* path);
	void SetExists(bool exists);
	void SetSize(uint64_t size);
	void SetSHA1(wchar_t* sha1);
	void SetMD5(wchar_t* md5);
	FileOpenMessage();
private:
	void WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer) ;
};

class FileCloseMessage :public LogMessage {
private:
	bool exists;
	bool marked_for_deletion;
	wchar_t path[MAX_PATH];
	uint64_t size;
	wchar_t sha1[41];
	wchar_t md5[33];
public:
	void SetPath(wchar_t* path);
	void SetExists(bool exists);
	void SetMarkedForDeletion(bool marked_for_deletion);
	void SetSize(uint64_t size);
	void SetSHA1(wchar_t* sha1);
	void SetMD5(wchar_t* md5);
	FileCloseMessage();
private:
	void WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer);
};

class FileRenameMessage :public LogMessage {
private:
	wchar_t o_path[MAX_PATH];
	wchar_t f_path[MAX_PATH];
	uint64_t size;
	wchar_t sha1[41];
	wchar_t md5[33];

public:
	void SetOPath(wchar_t* path);
	void SetFPath(wchar_t* path);
	void SetSize(uint64_t size);
	void SetSHA1(wchar_t* sha1);
	void SetMD5(wchar_t* md5);

	FileRenameMessage();
private:
	void WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer);
};

class FileDeleteMessage :public LogMessage {
private:
	wchar_t path[MAX_PATH];
	uint64_t size;
	wchar_t sha1[41];
	wchar_t md5[33];
	bool existed;

public:
	void SetPath(wchar_t* path);
	void SetExists(bool exists);
	void SetSize(uint64_t size);
	void SetSHA1(wchar_t* sha1);
	void SetMD5(wchar_t* md5);

	FileDeleteMessage();
private:
	void WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer);
};

class RegistryNewKeyMessage :public LogMessage {
private:
	wchar_t path[MAX_PATH];

public:
	void SetPath(wchar_t* path);
	RegistryNewKeyMessage();
private:
	void WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer);
};

class RegistryDeleteKeyMessage :public LogMessage {
private:
	wchar_t path[MAX_PATH];

public:
	void SetPath(wchar_t* path);
	RegistryDeleteKeyMessage();
private:
	void WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer);
};

class RegistryRenamedKeyMessage :public LogMessage {
private:
	wchar_t new_path[MAX_PATH];
	wchar_t old_path[MAX_PATH];

public:
	void SetOldPath(wchar_t* path);
	void SetNewPath(wchar_t* path);
	RegistryRenamedKeyMessage();
private:
	void WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer);
};

class RegistryNewValueMessage :public LogMessage {
private:
	wchar_t key_path[MAX_PATH];
	std::wstring value_name;
	unsigned long value_type;
	BYTE* data;
	unsigned long data_len;
public:
	void SetKeyPath(wchar_t* key_path);
	void SetValueName(wchar_t* value_name);
	void SetValueData(unsigned long value_type, void* data, unsigned long data_len);

	RegistryNewValueMessage();
	~RegistryNewValueMessage();
private:
	void WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer);
};

class RegistryDeletedValueMessage :public LogMessage {
private:
	wchar_t key_path[MAX_PATH];
	std::wstring old_value_name;
	unsigned long old_value_type;
	BYTE* old_data;
	unsigned long old_data_len;
public:
	void SetOldKeyPath(wchar_t* key_path);
	void SetOldValueName(wchar_t* value_name);
	void SetOldValueData(unsigned long value_type, void* data, unsigned long data_len);

	RegistryDeletedValueMessage();
	~RegistryDeletedValueMessage();
private:
	void WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer);
};

typedef struct KEY_VALUE_NAME{
	std::wstring value_name;
	unsigned long value_type;
	BYTE* data;
	unsigned long data_len;
} KeyValueName;
class RegistryModifiedValueMessage :public LogMessage {
private:
	wchar_t key_path[MAX_PATH];
	KeyValueName old_data;
	KeyValueName new_data;

public:
	void SetKeyPath(wchar_t* key_path);
	void SetOldValueName(wchar_t* value_name);
	void SetOldValueData(unsigned long value_type, void* data, unsigned long data_len);
	void SetNewValueName(wchar_t* value_name);
	void SetNewValueData(unsigned long value_type, void* data, unsigned long data_len);

	RegistryModifiedValueMessage();
	~RegistryModifiedValueMessage();
private:
	void WriteJson(Writer<GenericStringBuffer< rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer);
	void RegistryModifiedValueMessage::WriteDataObject(Writer<GenericStringBuffer<rapidjson::UTF16<wchar_t>>, rapidjson::UTF16<wchar_t>, rapidjson::UTF16<wchar_t>>& writer, KeyValueName& val);
};