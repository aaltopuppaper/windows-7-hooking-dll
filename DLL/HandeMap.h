#pragma once
#include <Windows.h>
#include <iostream> 
#include <map>
#include <mutex>

typedef struct HANDLE_INFO {
	int count = 0;
	std::wstring name;
	// The following flag identifies when a specific file handle has been marked for deletion
	bool marked_for_deletion = false;
} FileHandleInfo;

class HandleMap {
private:
	std::map<HANDLE, FileHandleInfo> map;
	std::recursive_mutex m;

public:
	void AddHandle(HANDLE h, TCHAR* fname, bool marked_for_deletion=false);
	bool SetHandleMarkedForDeletion(HANDLE h);
	bool GetPathFromHandle(HANDLE h, TCHAR* buff, SSIZE_T buff_size, bool* deletion_marker, bool pop=false);
};