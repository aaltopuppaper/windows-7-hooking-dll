/*
* Copyright (C) 2010-2015 Nektra S.A., Buenos Aires, Argentina.
* All rights reserved. Contact: http://www.nektra.com
*
*
* This file is part of Deviare In-Proc
*
*
* Commercial License Usage
* ------------------------
* Licensees holding valid commercial Deviare In-Proc licenses may use this
* file in accordance with the commercial license agreement provided with the
* Software or, alternatively, in accordance with the terms contained in
* a written agreement between you and Nektra.  For licensing terms and
* conditions see http://www.nektra.com/licensing/.  For further information
* use the contact form at http://www.nektra.com/contact/.
*
*
* GNU General Public License Usage
* --------------------------------
* Alternatively, this file may be used under the terms of the GNU
* General Public License version 3.0 as published by the Free Software
* Foundation and appearing in the file LICENSE.GPL included in the
* packaging of this file.  Please review the following information to
* ensure the GNU General Public License version 3.0 requirements will be
* met: http://www.gnu.org/copyleft/gpl.html.
*
**/

#include "stdafx.h"
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "NktHookLib.h"
#include "Dbghelp.h"

#pragma comment (lib, "dbghelp.lib")

#define DLL_64_PATH _T("C:\\Users\\Alberto Geniola\\Source\\Repos\\windows-7-hooking-dll\\Debug\\DLL_64.dll")
#define DLL_32_PATH _T("C:\\Users\\Alberto Geniola\\Source\\Repos\\windows-7-hooking-dll\\Debug\\DLL.dll")

//-----------------------------------------------------------

#if _MSC_VER >= 1910
#define X_LIBPATH "2017"
#elif _MSC_VER >= 1900
#define X_LIBPATH "2015"
#elif _MSC_VER >= 1800
#define X_LIBPATH "2013"
#elif _MSC_VER >= 1700
#define X_LIBPATH "2012"
#elif  _MSC_VER >= 1600
#define X_LIBPATH "2010"
#else
#define X_LIBPATH "2008"
#endif

#if defined _M_IX86
#ifdef _DEBUG
#pragma comment (lib, "NktHookLib_Debug.lib")
#else //_DEBUG
#pragma comment (lib, "NktHookLib.lib")
#endif //_DEBUG
#elif defined _M_X64
#ifdef _DEBUG
#pragma comment (lib, "NktHookLib64_Debug.lib")
#else //_DEBUG
#pragma comment (lib, "NktHookLib64.lib")
#endif //_DEBUG
#else
#error Unsupported platform
#endif

//-----------------------------------------------------------

static LPSTR ToAnsi(__in_z LPCWSTR sW);

/*
typedef BOOL(WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);
static BOOL MyIsWow64(HANDLE proc_handle)
{
	BOOL bIsWow64 = FALSE;

	//IsWow64Process is not available on all supported versions of Windows.
	//Use GetModuleHandle to get a handle to the DLL that contains the function
	//and GetProcAddress to get a pointer to the function if available.
	LPFN_ISWOW64PROCESS fnIsWow64Process = (LPFN_ISWOW64PROCESS)GetProcAddress(
		GetModuleHandle(TEXT("kernel32")), "IsWow64Process");

	if (NULL != fnIsWow64Process)
	{
		if (!fnIsWow64Process(proc_handle, &bIsWow64))
		{
			// We got an error here
			OutputDebugString(L"ERROR when invoking IsWow64Process");
			exit(2);
		}
	}
	return bIsWow64;
}
*/

static LPWSTR GetDLLToInject(LPWSTR szExeNameW) {
	/*
	// The handle must have the PROCESS_QUERY_INFORMATION or PROCESS_QUERY_LIMITED_INFORMATION access right.
	if (MyIsWow64(proc_handle))
		return DLL_64_PATH;
	else
		return DLL_32_PATH;
	*/
	PIMAGE_NT_HEADERS headers;
	HANDLE exe = CreateFile(szExeNameW, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (exe == INVALID_HANDLE_VALUE) {
		OutputDebugString(L"ERROR when opening target EXE file");
		exit(2);
	}

	// map the file to our address space
	// first, create a file mapping object
	HANDLE hMap = CreateFileMapping(
		exe,
		NULL,           // security attrs
		PAGE_READONLY,  // protection flags
		0,              // max size - high DWORD
		0,              // max size - low DWORD      
		NULL);         // mapping name - not used

					   // next, map the file to our address space
	void* mapAddr = MapViewOfFileEx(
		hMap,             // mapping object
		FILE_MAP_READ,  // desired access
		0,              // loc to map - hi DWORD
		0,              // loc to map - lo DWORD
		0,              // #bytes to map - 0=all
		NULL);         // suggested map addr

	headers = ImageNtHeader(mapAddr);
	if (headers == NULL) {
		if (exe != INVALID_HANDLE_VALUE)
			CloseHandle(exe);
		OutputDebugString(L"ERROR when getting header info of target EXE file");
		exit(2);
	}
	
	switch (headers->FileHeader.Machine) {
		case IMAGE_FILE_MACHINE_I386:
			if (mapAddr != INVALID_HANDLE_VALUE)
				UnmapViewOfFile(mapAddr);
			if (exe != INVALID_HANDLE_VALUE)
				CloseHandle(exe);
			return DLL_32_PATH;
			break;
		case IMAGE_FILE_MACHINE_AMD64:
			if (mapAddr != INVALID_HANDLE_VALUE)
				UnmapViewOfFile(mapAddr);
			if (exe != INVALID_HANDLE_VALUE)
				CloseHandle(exe);
			return DLL_64_PATH;
			break;
		default:
			if (exe != INVALID_HANDLE_VALUE)
				CloseHandle(exe);
			if (mapAddr != INVALID_HANDLE_VALUE)
				UnmapViewOfFile(mapAddr);
			OutputDebugString(L"ERROR Unsupported file provided.");
			exit(3);
	}


};

//-----------------------------------------------------------

int __CRTDECL wmain(__in int argc, __in wchar_t *argv[], __in wchar_t *envp[])
{
	DWORD dwOsErr, dwPid;
	LPSTR szInitFunctionA;
	LPWSTR szExeNameW = argv[1];

	OutputDebugStringW(L"Starting injected DLL");

	HANDLE dll_32_f = INVALID_HANDLE_VALUE;
	HANDLE dll_64_f = INVALID_HANDLE_VALUE;
	// Check params if they are correct
	dll_32_f = CreateFile(DLL_32_PATH, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (dll_32_f == INVALID_HANDLE_VALUE) {
		OutputDebugString(L"Error opening 32 bit DLL to be injected. Please MAKE SURE its path is correct.");
		exit(2);
	}

	dll_64_f = CreateFile(DLL_64_PATH, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (dll_64_f == INVALID_HANDLE_VALUE) {
		OutputDebugString(L"Error opening 64 bit DLL to be injected. Please MAKE SURE its path is correct.");
		exit(2);
	}

	//check arguments
	if (argc < 2)
	{
		wprintf_s(L"Use: InjectDLL path-to-dll [initialize-function-name]\n");
		return 1;
	}

	//assume a process path to execute
	dwPid = 0;

	szInitFunctionA = NULL;
	if (argc >= 4 && argv[3][0] != 0)
	{
		szInitFunctionA = ToAnsi(argv[3]);
		if (!szInitFunctionA)
		{
			wprintf_s(L"Error: Not enough memory.\n");
			return 1;
		}
	}

	//execute action
	STARTUPINFOW sSiW;
	PROCESS_INFORMATION sPi;

	memset(&sSiW, 0, sizeof(sSiW));
	sSiW.cb = (DWORD)sizeof(sSiW);
	memset(&sPi, 0, sizeof(sPi));

	dwOsErr = NktHookLibHelpers::CreateProcessWithDllW(szExeNameW, NULL, NULL, NULL, FALSE, 0, NULL, NULL, &sSiW, &sPi,
		GetDLLToInject(szExeNameW), NULL, szInitFunctionA);
	if (dwOsErr == ERROR_SUCCESS)
	{
		wprintf_s(L"Process #%lu successfully launched with dll injected!\n", sPi.dwProcessId);
		::CloseHandle(sPi.hThread);
		::CloseHandle(sPi.hProcess);
	}
	else
	{
		wprintf_s(L"Error %lu: Cannot launch process and inject dll.\n", dwOsErr);
	}

	free(szInitFunctionA);
	return (dwOsErr == ERROR_SUCCESS) ? 0 : 2;
}

//-----------------------------------------------------------

static LPSTR ToAnsi(__in_z LPCWSTR sW)
{
	int srcLen, destLen;
	LPSTR sA;

	srcLen = (int)wcslen(sW);
	if (!srcLen)
	{
		sA = (LPSTR)malloc(sizeof(CHAR));
		if (sA)
			*sA = 0;
		return sA;
	}
	destLen = ::WideCharToMultiByte(CP_ACP, WC_COMPOSITECHECK | WC_NO_BEST_FIT_CHARS, sW, srcLen, NULL, 0, NULL, NULL);
	sA = (LPSTR)malloc((destLen + 1) * sizeof(CHAR));
	if (!sA)
		return NULL;
	destLen = ::WideCharToMultiByte(CP_ACP, WC_COMPOSITECHECK | WC_NO_BEST_FIT_CHARS, sW, srcLen, sA, destLen, NULL, NULL);
	sA[destLen] = 0;
	return sA;
}