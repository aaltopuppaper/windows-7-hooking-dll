# Compile instructions

1. Clone the entire repository, by issuing the following command:

```
git clone --recursive https://albertogeniola@bitbucket.org/aaltopuppaper/windows-7-hooking-dll.git
```

1. Compile Deviare-In-Proc dependency. 
    1. Open a Visual Studio cmd-prompt as administrator.
    1. Navigate in windows-7-hooking-dll/deviare-in-proc
    1. Issue the following command in order to update the sub-module

```
    git submodule update --init
```

    1. Then compile DEVIARE-IN-PROC by issuing the following command

```
    cd src
    build.bat /MSVCVERSION 2015
```
    In case you are using a different version of Visual Studio, update the MSVCVERSION parameter accordingly. 
    THe compile script will process all the necessary compilations and will report the final state of the process. Ensure all compilations finish correctly, then move to the next step.

1. Open the main project file, APIMonitoring.sln with visual studio.
1. Ensure compilation dependencies are set correctly for all the projects of the solutions:

*Configuration Properties -> C/C++ -> General -> Additional Include Directories*:
$(SolutionExt)\deviare-in-proc\Include
$(SolutionExt)\rapid-json\include

*Configuration Properties -> Linker -> General -> Additional Library Directories*:
$(SolutionExt)\deviare-in-proc\Libs\2015

If the visual studio version used to compile Deviare-In-Proc was different, update the last part of the Additional Libraryu Directory accordingly.